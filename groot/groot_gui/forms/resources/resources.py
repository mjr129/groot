"""Resources names. File automatically generated by PyGuiSignal. Modifications may be overwritten."""
from mhelper.qt_resource_objects import ResourceIcon
database = ResourceIcon( ":/groot/database.svg" )
red_alignment = ResourceIcon( ":/groot/red_alignment.svg" )
red_pregraph = ResourceIcon( ":/groot/red_pregraph.svg" )
red_fusion = ResourceIcon( ":/groot/red_fusion.svg" )
remove = ResourceIcon( ":/groot/remove.svg" )
red_nrfg = ResourceIcon( ":/groot/red_nrfg.svg" )
red_consensus = ResourceIcon( ":/groot/red_consensus.svg" )
black_alignment = ResourceIcon( ":/groot/black_alignment.svg" )
green_minor = ResourceIcon( ":/groot/green_minor.svg" )
align = ResourceIcon( ":/groot/align.svg" )
red_edge = ResourceIcon( ":/groot/red_edge.svg" )
black_check = ResourceIcon( ":/groot/black_check.svg" )
red_minor = ResourceIcon( ":/groot/red_minor.svg" )
expand_right = ResourceIcon( ":/groot/expand_right.svg" )
black_domain = ResourceIcon( ":/groot/black_domain.svg" )
red_check = ResourceIcon( ":/groot/red_check.svg" )
green_domain = ResourceIcon( ":/groot/green_domain.svg" )
outgroup = ResourceIcon( ":/groot/outgroup.svg" )
find = ResourceIcon( ":/groot/find.svg" )
black_nrfg = ResourceIcon( ":/groot/black_nrfg.svg" )
red_domain = ResourceIcon( ":/groot/red_domain.svg" )
invert = ResourceIcon( ":/groot/invert.svg" )
green_split = ResourceIcon( ":/groot/green_split.svg" )
maximize = ResourceIcon( ":/groot/maximize.svg" )
black_minor = ResourceIcon( ":/groot/black_minor.svg" )
tree = ResourceIcon( ":/groot/tree.svg" )
move = ResourceIcon( ":/groot/move.svg" )
green_file = ResourceIcon( ":/groot/green_file.svg" )
green_alignment = ResourceIcon( ":/groot/green_alignment.svg" )
window = ResourceIcon( ":/groot/window.svg" )
text = ResourceIcon( ":/groot/text.svg" )
groot_logo = ResourceIcon( ":/groot/groot_logo.png" )
manchester_logo = ResourceIcon( ":/groot/manchester_logo.png" )
red_file = ResourceIcon( ":/groot/red_file.svg" )
between = ResourceIcon( ":/groot/between.svg" )
view = ResourceIcon( ":/groot/view.svg" )
fusion = ResourceIcon( ":/groot/fusion.svg" )
green_outgroup = ResourceIcon( ":/groot/green_outgroup.svg" )
green_subgraph = ResourceIcon( ":/groot/green_subgraph.svg" )
green_tree = ResourceIcon( ":/groot/green_tree.svg" )
save = ResourceIcon( ":/groot/save.svg" )
black_split = ResourceIcon( ":/groot/black_split.svg" )
genes = ResourceIcon( ":/groot/genes.svg" )
black_pregraph = ResourceIcon( ":/groot/black_pregraph.svg" )
samples_file = ResourceIcon( ":/groot/samples-file.svg" )
green_major = ResourceIcon( ":/groot/green_major.svg" )
help_cursor = ResourceIcon( ":/groot/help-cursor.svg" )
encompass_left = ResourceIcon( ":/groot/encompass_left.svg" )
legend = ResourceIcon( ":/groot/legend.svg" )
dismiss = ResourceIcon( ":/groot/dismiss.svg" )
help = ResourceIcon( ":/groot/help.svg" )
red_clean = ResourceIcon( ":/groot/red_clean.svg" )
red_major = ResourceIcon( ":/groot/red_major.svg" )
refresh = ResourceIcon( ":/groot/refresh.svg" )
black_data = ResourceIcon( ":/groot/black_data.svg" )
empty = ResourceIcon( ":/groot/empty.svg" )
choose = ResourceIcon( ":/groot/choose.svg" )
red_subgraph = ResourceIcon( ":/groot/red_subgraph.svg" )
domain = ResourceIcon( ":/groot/domain.svg" )
new = ResourceIcon( ":/groot/new.svg" )
black_subset = ResourceIcon( ":/groot/black_subset.svg" )
outside = ResourceIcon( ":/groot/outside.svg" )
wizard_logo = ResourceIcon( ":/groot/wizard_logo.png" )
align_left = ResourceIcon( ":/groot/align_left.svg" )
window_open = ResourceIcon( ":/groot/window_open.svg" )
internal = ResourceIcon( ":/groot/internal.svg" )
black_major = ResourceIcon( ":/groot/black_major.svg" )
shift_left = ResourceIcon( ":/groot/shift_left.svg" )
black_consensus = ResourceIcon( ":/groot/black_consensus.svg" )
shift_right = ResourceIcon( ":/groot/shift_right.svg" )
groot_file = ResourceIcon( ":/groot/groot-file.svg" )
database_monochrome = ResourceIcon( ":/groot/database_monochrome.svg" )
save_as = ResourceIcon( ":/groot/save_as.svg" )
encompass_right = ResourceIcon( ":/groot/encompass_right.svg" )
green_fusion = ResourceIcon( ":/groot/green_fusion.svg" )
red_subset = ResourceIcon( ":/groot/red_subset.svg" )
external = ResourceIcon( ":/groot/external.svg" )
select = ResourceIcon( ":/groot/select.svg" )
import_ = ResourceIcon( ":/groot/import_.svg" )
red_tree = ResourceIcon( ":/groot/red_tree.svg" )
workflow = ResourceIcon( ":/groot/workflow.svg" )
green_gene = ResourceIcon( ":/groot/green_gene.svg" )
green_pregraph = ResourceIcon( ":/groot/green_pregraph.svg" )
align_adjacent = ResourceIcon( ":/groot/align_adjacent.svg" )
red_data = ResourceIcon( ":/groot/red_data.svg" )
bbsrc_logo = ResourceIcon( ":/groot/bbsrc_logo.png" )
green_edge = ResourceIcon( ":/groot/green_edge.svg" )
window_closed = ResourceIcon( ":/groot/window_closed.svg" )
accept = ResourceIcon( ":/groot/accept.svg" )
green_check = ResourceIcon( ":/groot/green_check.svg" )
green_subset = ResourceIcon( ":/groot/green_subset.svg" )
settings = ResourceIcon( ":/groot/settings.svg" )
red_outgroup = ResourceIcon( ":/groot/red_outgroup.svg" )
split = ResourceIcon( ":/groot/split.svg" )
red_split = ResourceIcon( ":/groot/red_split.svg" )
colour = ResourceIcon( ":/groot/colour.svg" )
black_tree = ResourceIcon( ":/groot/black_tree.svg" )
open_file = ResourceIcon( ":/groot/open-file.svg" )
open = ResourceIcon( ":/groot/open.svg" )
red_gene = ResourceIcon( ":/groot/red_gene.svg" )
lego = ResourceIcon( ":/groot/lego.svg" )
black_clean = ResourceIcon( ":/groot/black_clean.svg" )
black_file = ResourceIcon( ":/groot/black_file.svg" )
black_outgroup = ResourceIcon( ":/groot/black_outgroup.svg" )
wizard = ResourceIcon( ":/groot/wizard.svg" )
no_entry = ResourceIcon( ":/groot/no-entry.svg" )
green_nrfg = ResourceIcon( ":/groot/green_nrfg.svg" )
new_file = ResourceIcon( ":/groot/new-file.svg" )
black_fusion = ResourceIcon( ":/groot/black_fusion.svg" )
black_subgraph = ResourceIcon( ":/groot/black_subgraph.svg" )
create = ResourceIcon( ":/groot/create.svg" )
expand_left = ResourceIcon( ":/groot/expand_left.svg" )
import_file = ResourceIcon( ":/groot/import-file.svg" )
green_data = ResourceIcon( ":/groot/green_data.svg" )
black_gene = ResourceIcon( ":/groot/black_gene.svg" )
black_edge = ResourceIcon( ":/groot/black_edge.svg" )
green_consensus = ResourceIcon( ":/groot/green_consensus.svg" )
default = ResourceIcon( ":/groot/default.css" )
rename = ResourceIcon( ":/groot/rename.svg" )
green_clean = ResourceIcon( ":/groot/green_clean.svg" )