import os
import shutil
from uuid import uuid4
from warnings import warn
import groot.core.data.config
from groot.core import constants
from groot.core.utilities import gr_file_system, gr_printing
from mhelper import file_helper


def run_in_temporary( function, *args, **kwargs ):
    """
    Sets the working directory to a temporary folder inside the current working directory.
    Calls `function`
    Then deletes the temporary folder and returns to the original working directory. 
    """
    #
    # Create and switch to temporary folder
    #
    id = uuid4()
    temp_folder_name = os.path.join( gr_file_system.storage.local_folder( constants.FOLDER_TEMPORARY ), "temporary_{}".format( id ) )
    
    if os.path.exists( temp_folder_name ):
        shutil.rmtree( temp_folder_name )
    
    file_helper.create_directory( temp_folder_name )
    os.chdir( temp_folder_name )
    
    #
    # Run the command
    #
    try:
        return function( *args, **kwargs )
    except Exception:
        for file in file_helper.list_dir( "." ):
            gr_printing.pr_information( "*** DUMPING FILE BECAUSE AN ERROR OCCURRED: {} ***".format( file  ) )
            for index, line in enumerate( file_helper.read_all_lines( file ) ):
                gr_printing.pr_information( "LINE {}: {} ".format( index, gr_printing.escape( line ) ) )
            gr_printing.pr_information( "*** END OF FILE ***" )
        
        raise
    finally:
        os.chdir( ".." )
        if groot.core.data.config.options().debug_external_tool:
            warn( "The directory '{}' has not been deleted because of the `debug_external_tool` flag.".format( temp_folder_name ), UserWarning )
        else:
            #
            # Remove temporary folder
            #
            shutil.rmtree( temp_folder_name )
