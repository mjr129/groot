from mhelper import file_helper
from typing import List, Set

from mhelper.disposal_helper import ManagedWith
from mhelper.mannotation import EFileMode, isFilename
from mhelper.special_types import MFlag

from groot.core import constants, Model
from groot.core.commands import workflow
from groot.core.constants import EFormat, Stage, STAGES, EChanges
from groot.core.utilities import gr_printing


class EImportFilter( MFlag ):
    """
    Mask on importing files.
    
    :cvar DATA: Data files (constants.EXT_FASTA, constants.EXT_BLAST)
    :cvar MODEL: Model files (constants.EXT_MODEL)
    """
    NONE = 0
    DATA: "EImportFilter" = 1 << 0
    MODEL: "EImportFilter" = 1 << 1


class Wizard:
    """
    Manages the guided wizard.
    
    !SERIALISABLE
    """
    __slots__ = ("model",
                 "new",
                 "name",
                 "imports",
                 "pauses",
                 "tolerance",
                 "alignment",
                 "tree",
                 "view",
                 "save",
                 "supertree",
                 "__stage",
                 "is_paused",
                 "is_completed",
                 "pause_reason",
                 "outgroups")
    
    
    def __init__( self,
                  model: Model,
                  new: bool,
                  name: str,
                  imports: List[str],
                  pauses: Set[Stage],
                  tolerance: int,
                  alignment: str,
                  tree: str,
                  view: bool,
                  save: bool,
                  outgroups: List[str],
                  supertree: str ):
        """
        CONSTRUCTOR.
        
        :param model:               Model to use (this can be `None`, in which case a new model is used)
        :param new:                 Create a new model?
                                    :values:`true→yes, false→no` 
        :param name:                Name the model?
                                    You can specify a complete path or just a name.
                                    If no name (empty) is specified, then the model is not saved.
                                    :values:`empty→no name`
        :param outgroups:           Outgroup accessions?
        :param imports:             Import files into the model?
        :param tolerance:           Component identification tolerance?
        :param alignment:           Alignment method?
                                    :values:`empty→default`
        :param supertree:           Supertree method? 
                                    :values:`empty→default`
        :param tree:                Tree generation method?
                                    :values:`empty→default`
        :param view:                View the final NRFG in Vis.js?
                                    :values:`true→yes, false→no` 
        :param save:                Save file to disk? (requires `name`)
                                    :values:`true→yes, false→no` 
        :param pauses:              Pause after stage default value.
        """
        self.model = model
        self.new = new
        self.name = name
        self.imports = imports
        self.pauses = pauses
        self.tolerance = tolerance
        self.alignment = alignment
        self.tree = tree
        self.view = view
        self.save = save
        self.supertree = supertree
        self.__stage = 0
        self.is_paused = True
        self.is_completed = False
        self.pause_reason = "start"
        self.outgroups = outgroups
        
        if self.save and not self.name:
            raise ValueError( "Wizard parameter `save` specified but `name` is not set." )
    
    
    def __str__( self ):
        r = []
        r.append( "new               = {}".format( self.new ) )
        r.append( "name              = {}".format( self.name ) )
        r.append( "imports           = {}".format( self.imports ) )
        r.append( "pauses            = {}".format( self.pauses ) )
        r.append( "tolerance         = {}".format( self.tolerance ) )
        r.append( "alignment         = {}".format( self.alignment ) )
        r.append( "tree              = {}".format( self.tree ) )
        r.append( "view              = {}".format( self.view ) )
        r.append( "stage             = {}".format( self.__stage ) )
        r.append( "is.paused         = {}".format( self.is_paused ) )
        r.append( "is.completed      = {}".format( self.is_completed ) )
        r.append( "pause_reason      = {}".format( self.pause_reason ) )
        r.append( "outgroups         = {}".format( self.outgroups ) )
        r.append( "save              = {}".format( self.save ) )
        r.append( "supertree         = {}".format( self.supertree ) )
        
        return "\n".join( r )
    
    
    def __pause( self, title: Stage, commands: tuple ) -> None:
        self.pause_reason = title
        gr_printing.pr_verbose( "Wizard has paused after `{}` due to user request.".format( title ) )
        gr_printing.pr_verbose( "Use the following commands to review:" )
        for command in commands:
            gr_printing.pr_verbose( "* `{}`".format( command.__name__ ) )
        gr_printing.pr_verbose( "Use the `{}` command to continue the wizard.".format( Wizard.step.__qualname__ ) )
        self.is_paused = True
    
    
    def __start_line( self, title: object ):
        title = "WIZARD: " + str( title )
        gr_printing.pr_section_start( title )
        return ManagedWith( on_exit = self.__end_line )
    
    
    def __end_line( self, title: object ):
        gr_printing.pr_section_end( title )
    
    
    def get_stage_name( self ):
        return self.__stages[self.__stage].__name__
    
    
    def step( self ) -> None:
        """
        Steps the Wizard forward until the next requested `pause`.
        If this completes the Wizard, `is_completed` will now return `True`.
        
        :return: Summary of changes made.
        """
        if self.is_completed:
            raise ValueError( "The wizard has already completed." )
        
        self.is_paused = False
        
        while not self.is_paused and self.__stage < len( self.__stages ):
            self.__stages[self.__stage]( self )
            self.__stage += 1
            self.__save_model()
        
        if self.__stage == len( self.__stages ):
            gr_printing.pr_verbose( "The wizard is complete." )
            self.is_completed = True
    
    
    def __fn8_make_splits( self ):
        with self.__start_line( STAGES.SPLITS_10 ):
            workflow.s100_splits.create_splits( self.model )
        
        if STAGES.SPLITS_10 in self.pauses:
            self.__pause( STAGES.SPLITS_10, (workflow.s100_splits.print_splits,) )
    
    
    def __fn9_make_consensus( self ):
        with self.__start_line( STAGES.CONSENSUS_11 ):
            workflow.s110_consensus.create_consensus( self.model )
        
        if STAGES.CONSENSUS_11 in self.pauses:
            self.__pause( STAGES.CONSENSUS_11, (workflow.s110_consensus.print_consensus,) )
    
    
    def __fn10_make_subsets( self ):
        with self.__start_line( STAGES.SUBSETS_12 ):
            workflow.s120_subsets.create_subsets( self.model )
        
        if STAGES.SUBSETS_12 in self.pauses:
            self.__pause( STAGES.SUBSETS_12, (workflow.s120_subsets.print_subsets,) )
    
    
    def __fn12_make_subgraphs( self ):
        with self.__start_line( "Subgraphs" ):
            algo = workflow.s140_supertrees.supertree_algorithms.get_algorithm( self.supertree )
            workflow.s140_supertrees.create_supertrees( self.model, algo )
        
        if STAGES.SUPERTREES_14 in self.pauses:
            self.__pause( STAGES.SUPERTREES_14, (workflow.s140_supertrees.print_supertrees,) )
    
    
    def __fn11_make_pregraphs( self ):
        with self.__start_line( "Pregraphs" ):
            workflow.s130_pregraphs.create_pregraphs( self.model )
        
        if STAGES.PREGRAPHS_13 in self.pauses:
            self.__pause( STAGES.PREGRAPHS_13, (workflow.s130_pregraphs.print_pregraphs,) )
    
    
    def __fn13_make_fused( self ):
        with self.__start_line( STAGES.FUSE_15 ):
            workflow.s150_fuse.create_fused( self.model )
        
        if STAGES.FUSE_15 in self.pauses:
            self.__pause( STAGES.FUSE_15, (workflow.s080_tree.print_trees,) )
    
    
    def __fn14_make_clean( self ):
        with self.__start_line( STAGES.CLEAN_16 ):
            workflow.s160_clean.create_cleaned( self.model )
        
        if STAGES.CLEAN_16 in self.pauses:
            self.__pause( STAGES.CLEAN_16, (workflow.s080_tree.print_trees,) )
    
    
    def __fn15_make_checks( self ):
        with self.__start_line( STAGES.CHECKED_17 ):
            workflow.s170_checked.create_checked( self.model )
        
        if STAGES.CHECKED_17 in self.pauses:
            self.__pause( STAGES.CHECKED_17, (workflow.s080_tree,) )
    
    
    def __fn16_view_nrfg( self ):
        if self.view:
            workflow.s080_tree.print_trees( self.model,
                                            graph = self.model.fusion_graph_clean.graph,
                                            format = EFormat.VISJS,
                                            file = "open" )
    
    
    def __fn7_make_fusions( self ):
        # Make fusions
        with self.__start_line( STAGES.FUSIONS_9 ):
            workflow.s090_fusion_events.create_fusions( self.model )
        
        if STAGES.FUSIONS_9 in self.pauses:
            self.__pause( STAGES.FUSIONS_9, (workflow.s080_tree.print_trees, workflow.s090_fusion_events.print_fusions) )
    
    
    def __fn6_make_trees( self ):
        with self.__start_line( STAGES.TREES_8 ):
            model = self.model
            ogs = [model.genes[x] for x in self.outgroups]
            
            workflow.s055_outgroups.set_outgroups( self.model, ogs )
            
            algo = workflow.s080_tree.tree_algorithms.get_algorithm( self.tree )
            workflow.s080_tree.create_trees( self.model, algo )
        
        if STAGES.TREES_8 in self.pauses:
            self.__pause( STAGES.TREES_8, (workflow.s080_tree.print_trees,) )
    
    
    def __fn5_make_alignments( self ):
        with self.__start_line( STAGES.ALIGNMENTS_7 ):
            algo = workflow.s070_alignment.alignment_algorithms.get_algorithm( self.alignment )
            workflow.s070_alignment.create_alignments( self.model, algo )
        
        if STAGES.ALIGNMENTS_7 in self.pauses:
            self.__pause( STAGES.ALIGNMENTS_7, (workflow.s070_alignment.print_alignments,) )
    
    
    def __fn4_make_major( self ):
        with self.__start_line( STAGES.MAJOR_4 ):
            workflow.s040_major.create_major( self.model, self.tolerance )
        
        if STAGES.MAJOR_4 in self.pauses:
            self.__pause( STAGES.MAJOR_4, (workflow.s020_sequences.print_genes, workflow.s040_major.print_major) )
    
    
    def __fn4_make_minor( self ):
        with self.__start_line( STAGES.MINOR_5 ):
            workflow.s050_minor.create_minor( self.model, self.tolerance )
        
        if STAGES.MINOR_5 in self.pauses:
            self.__pause( STAGES.MINOR_5, (workflow.s020_sequences.print_genes, workflow.s050_minor.print_minor) )
    
    
    def __fn4b_make_domains( self ):
        with self.__start_line( STAGES.DOMAINS_6 ):
            algo = workflow.s060_userdomains.domain_algorithms.get_algorithm( "component" )
            workflow.s060_userdomains.create_domains( self.model, algo )
    
    
    def __fn3_import_data( self ):
        with self.__start_line( STAGES.SEQ_AND_SIM_ps ):
            for import_ in self.imports:
                import_file( self.model, import_ )
        
        if STAGES.SEQ_AND_SIM_ps in self.pauses:
            self.__pause( STAGES.SEQ_AND_SIM_ps, (workflow.s020_sequences.print_genes,) )
    
    
    def __save_model( self ):
        if self.save:
            with self.__start_line( STAGES.FILE_1 ):
                workflow.s010_file.file_save( self.model, self.name )
    
    
    def __fn1_new_model( self ):
        # Start a new model
        with self.__start_line( "New" ):
            if self.new:
                workflow.s010_file.file_new( self.model )
    
    
    __stages = [__fn1_new_model,
                __fn3_import_data,
                __fn4_make_major,
                __fn4_make_minor,
                __fn4b_make_domains,
                __fn5_make_alignments,
                __fn6_make_trees,
                __fn7_make_fusions,
                __fn8_make_splits,
                __fn9_make_consensus,
                __fn10_make_subsets,
                __fn11_make_pregraphs,
                __fn12_make_subgraphs,
                __fn13_make_fused,
                __fn14_make_clean,
                __fn15_make_checks,
                __fn16_view_nrfg]


def create_components( model: Model, tol: int = 0 ) -> None:
    """
    Executes `create_major` then `create_minor`.
    
    cat: create
    :param tol: Tolerance
    """
    workflow.s040_major.create_major( model, tol )
    workflow.s050_minor.create_minor( model, tol )


def drop_components( model: Model ) -> None:
    """
    Removes all the components from the model.
    
    cat: drop
    """
    workflow.s050_minor.drop_minor( model )
    
    count = len( model.components )
    workflow.s040_major.drop_major( model )
    
    gr_printing.pr_verbose( "Dropped all {} components from the model.".format( count ) )
    
    model.ui_hint |= EChanges.COMPONENTS


def import_file( model: Model,
                 file_name: isFilename[EFileMode.READ],
                 skip_bad_extensions: bool = False,
                 filter: EImportFilter = EImportFilter.DATA,
                 query: bool = False ) -> None:
    """
    Imports a file.
    _How_ the file is imported is determined by its extension.

        `.groot`     --> `file_load`
        `.fasta`     --> `import_fasta`
        `.blast`     --> `import_blast`
        `.composite` --> `import_composite`
        `.imk`       --> `source` (runs the script)    
        
    cat: import
     
    :param file_name:               Name of file to import. 
    :param skip_bad_extensions:     When set, if the file has an extension we don't recognise, no error is raised. 
    :param filter:                  Specifies what kind of files we are allowed to import.
    :param query:                   When set the kind of the file is printed to `sys.stdout` and the file is not imported. 
    :return:                        Nothing is returned, the file data is incorporated into the model and messages are sent via `sys.stdout`.
    """
    ext = file_helper.get_extension( file_name ).lower()
    
    if filter.DATA:
        if ext == ".blast":
            if not query:
                workflow.s030_similarity.import_similarities( model, file_name )
                return
            else:
                gr_printing.pr_information( "BLAST: {}.".format( gr_printing.escape( file_name ) ) )
                model.ui_hint |= EChanges.INFORMATION
                return
        elif ext in (".fasta", ".fa", ".faa"):
            if not query:
                workflow.s020_sequences.import_genes( model, file_name )
                return
            else:
                gr_printing.pr_information( "FASTA: {}.".format( gr_printing.escape( file_name ) ) )
                model.ui_hint |= EChanges.INFORMATION
                return
    
    if filter.MODEL:
        if ext == constants.EXT_MODEL:
            if not query:
                workflow.s010_file.file_load( model, file_name )
                return
            else:
                gr_printing.pr_information( "Model: {}.".format( gr_printing.escape( file_name ) ) )
                model.ui_hint |= EChanges.INFORMATION
                return
    
    if skip_bad_extensions:
        return
    
    raise ValueError( "Cannot import the file '{}' because I don't recognise the extension '{}'.".format( file_name, ext ) )


def import_directory( model: Model,
                      directory: str,
                      query: bool = False,
                      filter: EImportFilter = EImportFilter.DATA,
                      reset: bool = True ) -> None:
    """
    Imports all importable files from a specified directory
    
    cat: import
    
    :param query:       Query the directory (don't import anything).
    :param reset:       Whether to clear data from the model first.
    :param directory:   Directory to import
    :param filter:      Filter on import
    """
    if reset:
        if not query:
            workflow.s010_file.file_new( model )
        else:
            gr_printing.pr_information( "Importing will start a new model." )
    
    contents = file_helper.list_dir( directory )
    
    if filter & EImportFilter.DATA:
        for file_name in contents:
            import_file( model, file_name, skip_bad_extensions = True, filter = EImportFilter.DATA, query = query )
    
    if not query:
        if reset:
            model.ui_hint |= EChanges.MODEL_OBJECT
        else:
            model.ui_hint |= EChanges.MODEL_ENTITIES
