"""
Algorithms for user-domains

Used for display, nothing to do with the model.
"""
from typing import Callable

from groot.core import Model
from groot.core.utilities import gr_printing
from mhelper import ansi, string_helper, exception_helper

from groot.core.data import Gene
from groot.core.constants import EChanges
from groot.core.utilities import cli_view_utils
from groot.core.utilities.extendable_algorithm import AlgorithmCollection


DAlgorithm = Callable[[Gene], str]
"""A delegate for a function that takes a sequence and an arbitrary parameter, and produces an list of domains."""

domain_algorithms = AlgorithmCollection( DAlgorithm, "Domain" )


def create_domains( model: Model, algorithm: domain_algorithms.Algorithm ):
    """
    Creates the domains.
    Existing domains are always replaced.
    Domains are only used for viewing and have no bearing on the actual calculations.
    
    cat: create
    
    :param algorithm:   Mode of domain generation. See `algorithm_help`.
    """
    if not model.genes:
        raise ValueError( "Cannot generate domains because there are no sequences." )
    
    model.user_domains.clear()
    
    for sequence in model.genes:
        for domain in algorithm( sequence ):
            model.user_domains.add( domain )
    
    gr_printing.pr_verbose( "Domains created, there are now {} domains.".format( len( model.user_domains ) ) )
    
    model.ui_hint |= EChanges.DOMAINS


def drop_domains( model: Model, ):
    """
    Removes the user-domains from the model.
    
    cat: drop
    """
    model.user_domains.clear()


def print_domains( model: Model, algorithm: domain_algorithms.Algorithm ):
    """
    Prints the genes (highlighting components).
    Note: Use :func:`print_fasta` or :func:`print_alignments` to show the actual sites.
    
    cat: print
    
    :param model:           Model to print for.
    :param algorithm:       How to break up the sequences. See `domain_algorithms`.
    """
    if not isinstance( algorithm, domain_algorithms.Algorithm ):
        raise exception_helper.type_error( "algorithm", algorithm, domain_algorithms.Algorithm )
    
    longest = max( x.length for x in model.genes )
    r = []
    
    for sequence in model.genes:
        minor_components = model.components.find_components_for_minor_gene( sequence )
        
        if not minor_components:
            minor_components = [None]
        
        for component_index, component in enumerate( minor_components ):
            if component_index == 0:
                r.append( sequence.accession.ljust( 20 ) )
            else:
                r.append( "".ljust( 20 ) )
            
            if component:
                r.append( cli_view_utils.component_to_ansi( component ) + " " )
            else:
                r.append( "Ø " )
            
            subsequences = __list_userdomains( sequence, algorithm )
            
            for subsequence in subsequences:
                components = model.components.find_components_for_minor_domain( subsequence )
                
                if component in components:
                    colour = cli_view_utils.component_to_ansi_back( component )
                else:
                    colour = ansi.BACK_LIGHT_BLACK
                
                size = max( 1, int( (subsequence.length / longest) * 80 ) )
                name = "{}-{}".format( subsequence.start, subsequence.end )
                
                r.append( colour +
                          ansi.DIM +
                          ansi.FORE_BLACK +
                          "▏" +
                          ansi.NORMAL +
                          string_helper.centre_align( name, size ) +
                          ansi.RESET )
            
            r.append( "\n" )
        
        r.append( "\n" )
    
    print( "".join( r ) )
    model.ui_hint |= EChanges.INFORMATION


def __list_userdomains( sequence: Gene, algorithm: domain_algorithms.Algorithm ):
    assert isinstance( algorithm, domain_algorithms.Algorithm ), algorithm
    return algorithm( sequence )
