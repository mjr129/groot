from groot.mgraph import MGraph, MNode
from mhelper import log_helper


class FtModel:
    """
    Represents the current workspace.
    """
    
    def __init__( self ):
        self.tree: MGraph = MGraph()
        self.selected: MNode = None
    
    
    def reset( self ):
        self.tree = MGraph()
        self.selected = None


default_model: FtModel = FtModel()
print = log_helper.Logger("faketree", True)