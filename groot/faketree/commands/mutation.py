from typing import List

from groot.faketree.model.mutators import CombinationMutation, MutationWithPool, RandomMutation, RandomMutationRoot, SeqgenMutation, UniqueMutation, UniqueMutationRoot
from groot.faketree.model.treedata import NodeData, MftNode
from groot.faketree.model import workspace
from mhelper import file_helper






def __send_to_target( target, r ):
    text = "\n".join( r )
    
    if target is None:
        workspace.print( text )
    else:
        file_helper.write_all_text( target, text )
        workspace.print( "{} bytes sent to “{}”".format( len( text ), target ) )



def generate( verbose: bool = False ):
    """
    Applies the current mutators and generates the sequences.
    
    :param verbose: Verbose output
    """
    for node in workspace.default_model.tree:
        if node.data.mutator is None:
            raise ValueError( "The node {} has no mutator assigned.".format( node ) )
        
        node.data.mutator.reset()
    
    n = 0
    
    all_good = False
    
    while not all_good:
        n += 1
        if verbose:
            workspace.print( "***** ITERATION {} *****".format( n ) )
        
        all_good = True
        any_changed = False
        
        for node in workspace.default_model.tree:
            if node.data.sequence:
                continue
            
            if node.data.mutator.mutate():
                any_changed = True
                
                if verbose:
                    if node.data.sequence:
                        workspace.print( "{}".format( node ) )
                    else:
                        workspace.print( "{}...".format(node) )
            
            if not node.data.sequence:
                all_good = False
        
        if not all_good and not any_changed:
            raise ValueError( "Iteration occurred but nothing changed. Check your mutators." )
    
    workspace.print( "Applied. {} of {} nodes have a sequence.".format( sum( 1 for node in workspace.default_model.tree if node.data.sequence is not None ), len( workspace.default_model.tree ) ) )


def set_pool( pool: str = "" ):
    # noinspection SpellCheckingInspection
    """
        Displays or modifies the default site pool.
        This is used as the default value for some functions.
        
        :param pool:    New value for the site pool.
                        If empty the pool is displayed but not changed.
                        You can also use a single letter to specify an inbuilt pool: `D`NA or `P`rotein.
    """
    pool = pool.upper()
    if pool == "D":
        pool = "ACGT"
    elif pool == "P":
        pool = "ACEFGHIKLMNPQRSTVWY"
    elif len( pool ) == 1:
        raise ValueError( "Pool must have at least two site possibilities." )
    
    if pool:
        MutationWithPool.DEFAULT_POOL = pool
        workspace.print( "POOL CHANGED TO `{}`".format( pool ) )
    else:
        workspace.print( "POOL IS `{}`".format( pool ) )


def make_composite_node( nodes: List[MftNode] ):
    """
    Specifies that a node originates through a composite mutation event.
    The node should have multiple parents for this to make sense.
    :param nodes:    Node(s) to set this mutator on.     
    """
    for node in nodes:
        if node.num_parents <= 1:
            raise ValueError( "Cannot make the node «{}» composite because composite nodes must have at least two parents." )
        
        data: NodeData = node.data
        data.mutator = CombinationMutation( node )


def make_unique_root( nodes: List[MftNode], origin: str = "", amplitude: int = 1 ):
    """
    Sets a node to use the `UniqueMutationRoot` mutator.
    - Defines a sequence with enough sites for a unique mutation for every descendant with a `UniqueMutation` mutator.
    
    :param nodes:       Node(s) to set this mutator on.     
    :param origin:      Initial site pool. An empty string denotes the default pool.
    :param amplitude:   Multiplier on the length of the sequence. e.g. If this is `2` every child will get 2 unique mutations in this sequence.
    :return: 
    """
    for node in nodes:
        node.data.mutator = UniqueMutationRoot( node, origin, amplitude )
    
    workspace.print( "{} roots unique.".format( len( nodes ) ) )


def make_unique_node( nodes: List[MftNode], new_site: str = "", amplitude: int = 1 ):
    """
    Sets a node to use the `UniqueMutation` mutator.
    - Changes 1 site descending from any and all parents with a `UniqueMutationRoot` mutator.
     
    :param nodes:       Node(s) to set this mutator on. 
    :param new_site:    Mutated site. An empty string denotes the default pool.
    :param amplitude:   Multiplier on the number of sites to mutate. e.g. If this is `2` this will change 2 unique mutations from its ancestor(s).
    """
    for node in nodes:
        node.data.mutator = UniqueMutation( node, new_site, amplitude )
    
    workspace.print( "{} nodes now unique.".format( len( nodes ) ) )


def make_seqgen_node( nodes: List[MftNode], parameters: str = "", edge_length: int = 1, seq_length: int = 1000, uid: object = 1 ):
    """
    Sets a node to use the `SeqgenMutation` mutator.
    Uses Seqgen to perform the mutations.
     
    :param uid:                 One SeqGen call will be made for all sub-trees whose nodes share a common UID.
                                You can set the UID manually to ensure parts of the tree share the same SeqGen call (or not).
                                Note that same SeqGen call will never be made for parts of the tree which do not connect or which possess different parameters.
                                The default for `mk_seqgen` is 1.
    :param seq_length:          Length of randomly generated sequences (ignored if the genes have an ancestor from which they derive).
    :param edge_length:         Length of edge into this node 
    :param nodes:               Node(s) to set this mutator on. 
    :param parameters:          Parameter string passed to Seq-Gen.
    """
    for node in nodes:
        node.data.mutator = SeqgenMutation( node, edge_length, seq_length, parameters, uid )
    
    workspace.print( "{} nodes now using seqgen.".format( len( nodes ) ) )


def make_random_node( nodes: List[MftNode], pool: str = "", chance = 0.05 ):
    """
    Sets a node to use the `RandomMutation` mutator.
    - Randomly changes all sites (coming from any parent).
     
    :param nodes:   Node(s) to set this mutator on. 
    :param pool:    Site pool. An empty string denotes the default pool. 
    :param chance:  Chance of mutation per site.
    """
    for node in nodes:
        node.data.mutator = RandomMutation( node, chance, pool )
    
    workspace.print( "{} nodes now random.".format( len( nodes ) ) )


def make_random_root( nodes: List[MftNode], pool: str = "", length = 1000 ):
    """
    Sets a node to use the `RandomMutation` mutator.
    - Randomly changes all sites (coming from any parent).
     
    :param nodes:   Node(s) to set this mutator on. 
    :param pool:    Site pool. An empty string denotes the default pool. 
    :param length:  Length of sequence.
    """
    for node in nodes:
        node.data.mutator = RandomMutationRoot( node, length, pool )
    
    workspace.print( "{} nodes now random.".format( len( nodes ) ) )


def make_unique( roots: List[MftNode], origin: str = "", new_site: str = "", amplitude: int = 1, d_amplitude: int = 1 ):
    """
    Applies `mk_unique_root` to the root nodes and `mk_unique` to all descendants.
     
    :param roots:       Root node(s). These nodes and all descendants will have their mutators set.
    :param origin:      Parameter on `mk_unique_root`.
    :param new_site:    Parameter on `mk_unique`.
    :param amplitude:   Parameter on `mk_unique_root`.
    :param d_amplitude: Parameter on `mk_unique`.  
    """
    descendants = __acquire_set( roots )
    make_unique_root( roots, origin, amplitude )
    make_unique_node( descendants, new_site, d_amplitude )



def make_seqgen( roots: List[MftNode], parameters: str = "", uid: object = None ):
    """
    Applies `mk_seqgen` to the root nodes and descendants.
     
    :param roots:           Root node(s). These nodes and all descendants will have their mutators set.
    :param parameters:      Parameter on `mk_seqgen`.
    :param uid:             Parameter on `mk_seqgen`.
                            The default is a unique value for each call to this function (`seqgen`).
    """
    descendants = __acquire_set( roots )
    
    if uid is None:
        uid = object()
        
    make_seqgen_node( roots, parameters, uid = uid )
    make_seqgen_node( descendants, parameters, uid = uid )


def make_random( roots: List[MftNode], length = 1000, pool: str = "", chance = 0.05 ):
    """
    Applies `mk_random_root` to the root nodes and `mk_random` to all descendants.
    
    :param roots:     Root node(s). These nodes and all descendants will have their mutators set.
    :param pool:      Parameter on `mk_random` and `mk_random_root`.
    :param length:    Parameter on `mk_random_root`. 
    :param chance:    Parameter on `mk_random`.
    """
    descendants = __acquire_set( roots )
    make_random_root( roots, pool, length )
    make_random_node( descendants, pool, chance )


def __acquire_set( nodes ):
    """
    Acquires the set of nodes.
    :param nodes: roots
    :return: descendants
    """
    descendants = []
    for node in nodes:
        descendants.extend( node.list_descendants() )
    return descendants
