from os import path
from typing import Optional

from groot.core.utilities import gr_printing, gr_file_system
from mhelper import file_helper, io_helper
from mhelper.mannotation import EFileMode, isFilename, isOptional

import sys

from groot.core import constants
from groot.core.commands.gimmicks import wizard
from groot.core.data import config, sample_data
from groot.core.data.model import Model
from groot.core.constants import EChanges


def file_new( model: Model ) -> None:
    """
    Starts a new model
    
    cat: file
    
    :param model: Model to clear
    """
    model.__init__()
    gr_printing.pr_verbose( "Model cleared." )
    
    model.ui_hint |= EChanges.MODEL_OBJECT


def file_save( model: Model = None, file_name: isOptional[isFilename[EFileMode.WRITE, constants.EXT_MODEL]] = None ) -> None:
    """
    Saves the model
    
    cat: file
    
    :param model: Model to manipulate.
    :param file_name: Filename. File to load. Either specify a complete path, or the name of the file in the `sessions` folder. If not specified the current filename is used.
    :return: 
    """
    model = model
    
    if file_name:
        file_name = __fix_path( file_name )
    else:
        file_name = model.file_name
    
    if not file_name:
        raise ValueError( "Cannot save because a filename has not been specified." )
    
    config.remember_file( file_name )
    
    sys.setrecursionlimit( 10000 )
    
    with gr_printing.pr_action( "Saving file to «{}»".format( file_name ) ):
        model.file_name = file_name
        io_helper.save_binary( file_name, model )
    
    model.file_name = file_name
    gr_printing.pr_verbose( "Saved model to {}", file_name )
    
    model.ui_hint |= EChanges.FILE_NAME


def export_json( model: Model, file_name: isFilename[EFileMode.WRITE, constants.EXT_JSON] ) -> None:
    """
    Exports the entirety of the current model into a JSON file for reading by external programs.
    
    cat: advanced, file
    
    :param model: Model to manipulate.
    :param file_name:   Name of file to export to.
    """
    io_helper.save_json_pickle( file_name, model )


def file_load( model: Model, file_name: isFilename[EFileMode.READ] ) -> None:
    """
    Loads the model from a file
    
    cat: file
    
    :param model: Model to use
    :param file_name:   File to load.
                        If you don't specify a path, the following folders are attempted (in order):
                        * The current working directory
                        * `$(DATA_FOLDER)sessions`
    """
    
    if path.isfile( file_name ):
        file_name = path.abspath( file_name )
    else:
        file_name = __fix_path( file_name )
    
    try:
        new_model: Model = io_helper.load_binary( file_name, type_ = Model )
    except Exception as ex:
        raise ValueError( "Failed to load the model «{}». Either this is not a Groot model or this model was saved using a different version of Groot.".format( file_name ) ) from ex
    
    for x in Model.__slots__:
        if x.startswith( "__" ):
            x = "_Model" + x
        setattr( model, x, getattr( new_model, x ) )
    
    model.file_name = file_name
    
    config.remember_file( file_name )
    gr_printing.pr_verbose( "Loaded model: {}".format( file_name ) )
    
    model.ui_hint |= EChanges.MODEL_OBJECT


def print_sample_files() -> None:
    """
    Lists the available samples, or loads the specified sample.
    
    cat: file
    """
    for sample_dir in sample_data.get_samples():
        gr_printing.pr_information( file_helper.get_filename( sample_dir ) )
    else:
        gr_printing.pr_information( "No samples available. Please download and add sample data to `{}`.".format( sample_data.get_sample_data_folder() ) )


def file_load_sample( model: Model, name: Optional[str] = None, query: bool = False ) -> None:
    """
    Lists the available samples, or loads the specified sample.
    
    cat: file
    
    :param name:    Name of sample. 
    :param query:   When set the sample is viewed but not loaded. 
    :return: 
    """
    file_name = path.join( sample_data.get_sample_data_folder(), name )
    
    if not path.isdir( file_name ):
        raise ValueError( "'{}' is not a valid sample directory.".format( name ) )
    
    if not query:
        gr_printing.pr_verbose( "Loading sample dataset «{}».".format( file_name ) )
    else:
        gr_printing.pr_information( "Sample data: «{}».".format( file_name ) )
    
    wizard.import_directory( model,
                             file_name,
                             filter = wizard.EImportFilter.DATA,
                             query = query )


def file_load_last( model: Model ):
    """
    Loads the last file from the recent list.
    
    cat: file
    """
    if not config.options().recent_files:
        raise ValueError( "Cannot load the last session because there are no recent sessions." )
    
    file_load( model, config.options().recent_files[-1].file_name )


def file_recent():
    """
    Prints the contents of the `sessions` folder
    
    cat: file
    """
    r = []
    
    r.append( "SESSIONS:" )
    for file in sample_data.get_workspace_files():
        r.append( file )
    
    r.append( "\nRECENT:" )
    for file in reversed( config.options().recent_files ):
        r.append( file )
    
    gr_printing.pr_information( "\n".join( r ) )


def __fix_path( file_name: str ) -> str:
    """
    Adds the directory to the filename, if not specified.
    """
    if path.sep not in file_name:
        file_name = path.join( gr_file_system.storage.local_folder( "sessions" ), file_name )
    
    if not file_helper.get_extension( file_name ):
        file_name += constants.EXT_MODEL
    
    return file_name
