from Bio.PopGen.GenePop import Controller

from groot.core.data import config
from groot.mgraph import NodeStyle

from groot.core import constants
from groot.core.utilities import AlgorithmCollection, gr_printing


def groot():
    """
    Displays the application version.
    
    Also has the secondary affect of loading all the options from disk.
    
    cat: advanced, extra
    """
    print( "I AM {}. VERSION {}.".format( Controller.ACTIVE.app.name, Controller.ACTIVE.app.version ) )
    _ = config.options()


def help_tree_node_formatting() -> str:
    """
    Help on tree-node formatting.
    """
    return str( NodeStyle.replace_placeholders.__doc__ )





def help_algorithms():
    """
    Prints available algorithms.
    """
    r = []
    for collection in AlgorithmCollection.ALL:
        r.append( "" )
        r.append( gr_printing.fmt_section_start( collection.name ) )
        
        for name, function in collection:
            if name != "default":
                r.append( gr_printing.fmt_code( name ) )
                r.append( gr_printing.fmt_rst( function.__doc__ ) )
                r.append( "" )
        
        r.append( "" )
    
    return "\n".join( r )

