from groot.core import Model
from groot.core.data import INamedGraph, FixedUserGraph




def import_graph( model:Model,graph: INamedGraph, name: str = "" ):
    """
    Imports a graph or tree as a user-graph.
    
    User graphs are graphs that can be viewed and explored but do not, by default, form part of the model.
    
    cat: import
    
    :param model: Model to manipulate.
    :param graph:   Graph to import. See `format_help`. If you specify an existing user-graph or a graph
                    that is already part of the model, a copy will be made. 
    :param name:    Name of the graph. If not provided your graph will be assigned a default name. 
    """
    
    graph = FixedUserGraph( graph.graph.copy(), name or "usergraph{}".format( len( model.user_graphs ) ) )
    
    model.user_graphs.append( graph )



def drop_graph(model:Model, graph: INamedGraph ):
    """
    Removes a graph created with `import_graph`.
    
    cat: drop
    
    :param model: Model to manipulate.
    :param graph:   Graph to remove. See `format_help`.
    """
    
    if not isinstance( graph, FixedUserGraph ):
        raise ValueError( "The specified graph is not a user-graph and cannot be removed. Please specify an _existing_ user-graph." )
    
    model.user_graphs.remove( graph )