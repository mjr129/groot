from typing import Callable, List, Optional
from groot.core.utilities import gr_printing as gr_printing
from mhelper import io_helper, string_helper

from groot.core.data import Component, Model
from groot.core.constants import EChanges
from groot.core.utilities import cli_view_utils, external_runner
from groot.core.utilities.extendable_algorithm import AlgorithmCollection


DAlgorithm = Callable[[Model, str], str]
"""A delegate for a function that takes a model and unaligned FASTA data, and produces an aligned result, in FASTA format."""

alignment_algorithms = AlgorithmCollection( DAlgorithm, "Alignment" )



def create_alignments( model:Model,algorithm: alignment_algorithms.Algorithm, component: Optional[List[Component]] = None ) :
    """
    Aligns the component.
    If no component is specified, aligns all components.
    
    Requisites: `create_minor` and FASTA data.
    
    cat: create
    
    :param model: Model to use
    :param algorithm:   Algorithm to use. See `algorithm_help`.
    :param component:   Component to align, or `None` for all.
    """
    
    if not all( x.site_array for x in model.genes ):
        raise ValueError( "Refusing to make alignments because there is no site data. Did you mean to load the site data (FASTA) first?" )
    
    to_do = cli_view_utils.get_component_list( model, component )
    before = sum( x.alignment is not None for x in model.components )
    
    for component_ in gr_printing.pr_iterate( to_do, title = "Aligning" ):
        fasta = component_.get_unaligned_legacy_fasta()
        component_.alignment = external_runner.run_in_temporary( algorithm, component_.model, fasta )
    
    after = sum( x.alignment is not None for x in model.components )
    gr_printing.pr_verbose( "{} components aligned. {} of {} components have an alignment ({}).".format( len( to_do ), after, len( model.components ), string_helper.as_delta( after - before ) ) )
    
    model.ui_hint|= EChanges.COMP_DATA



def set_alignment( component: Component, alignment: str ) -> None:
    """
    Sets a component tree manually.
    
    cat: set
    
    :param component:        Component. 
    :param alignment:        Alignment to set. 
    """
    if component.alignment:
        raise ValueError( "This component already has an alignment. Did you mean to drop the existing alignment first?" )
    
    component.alignment = alignment
    
    component.model.ui_hint|= EChanges.COMP_DATA



def drop_alignment( model:Model, component: Optional[List[Component]] = None )->None :
    """
    Removes the alignment data from the component.
    If no component is specified, drops all alignments.
    
    cat: drop
    
    :param model: Model to use
    :param component: Component to drop the alignment for, or `None` for all.
    """
    to_do = cli_view_utils.get_component_list( model, component )
    count = 0
    
    for component_ in to_do:
        component_.alignment = None
        count += 1
    
    gr_printing.pr_verbose( "{} alignments removed across {} components.".format( count, len( to_do ) ) )
    
    model.ui_hint|= EChanges.COMP_DATA



def print_alignments( model:Model, component: Optional[List[Component]] = None, x = 1, n = 0, file: str = "" ) :
    """
    Prints the alignment for a component.
    
    cat: print
    
    :param model: Model to use
    :param file:        File to write to. See `file_write_help`. If this is empty then colours and headings are also printed. 
    :param component:   Component to print alignment for. If not specified prints all alignments.
    :param x:           Starting index (where 1 is the first site).
    :param n:           Number of sites to display. 0 assumes the default (80).
    """
    to_do = cli_view_utils.get_component_list( model, component )
    
    if not n:
        n = 80
    
    r = []
    
    colour = not file
    sec = colour or len( to_do ) > 1
    
    for component_ in to_do:
        if sec:
            r.append( "<section name='Component {}'>".format( gr_printing.escape( component_ ) ) )
        
        if component_.alignment is None:
            raise ValueError( "No alignment is available for this component. Did you remember to run `align` first?" )
        else:
            if colour:
                r.append( cli_view_utils.colour_fasta_ansi( component_.alignment, model.site_type, model, x, n ) )  # TODO: Should provide XML element defining the style instead of formatting within this function
            else:
                r.append( component_.alignment )
        
        if sec:
            r.append( "</section>" )
    
    with io_helper.open_write( file ) as file_out:
        file_out.write( "\n".join( r ) + "\n" )
    
    model.ui_hint|= EChanges.INFORMATION
