import os
from os import path
from typing import List

from groot.core import constants
from groot.core.utilities import gr_file_system
from mhelper import file_helper


def get_sample_contents( name: str ) -> List[str]:
    if not path.sep in name:
        name = path.join( get_sample_data_folder(), name )
    
    all_files = file_helper.list_dir( name )
    
    return [x for x in all_files if x.endswith( ".blast" ) or x.endswith( ".fasta" )]


def get_samples():
    """
    INTERNAL
    
    Obtains the list of samples
    """
    sample_data_folder = get_sample_data_folder()
    return file_helper.list_sub_dirs( sample_data_folder )


def get_workspace_files() -> List[str]:
    """
    INTERNAL
    
    Obtains the list of workspace files
    """
    r = []
    
    for file in os.listdir(  gr_file_system.storage.local_folder("sessions" ) ):
        if file.lower().endswith( constants.EXT_MODEL ):
            r.append( file )
    
    return r


def get_sample_data_folder( name: str = None ):
    """
    PRIVATE
    
    Obtains the sample data folder
    """
    sdf = gr_file_system.storage.local_folder( "sample_data" )
    
    if not name:
        return sdf
    
    if path.sep in name:
        return name
    
    return path.join( sdf, name )
