from typing import cast

from mhelper import log_helper, ansi_format_helper, utf_table


LOG = log_helper.Logger( "groot", True )
cast( log_helper.Handler, LOG.handler ).show_name = False
cast( log_helper.Handler, LOG.handler ).show_processes = False
cast( log_helper.Handler, LOG.handler ).show_threads = False


def pr_action( name ):
    return LOG.action( name )


def pr_verbose( *a, **k ):
    LOG( *a, **k )


def pr_information( *a, **k ):
    LOG( *a, **k )


def pr_section( name ):
    return LOG( name )


def pr_section_start( name ):
    LOG( "+{}".format( name ) )
    LOG.indent += 1


def pr_section_end( name ):
    LOG.indent -= 1
    LOG( "-{}".format( name or "" ) )


def pr_question( name, opts = () ):
    LOG( name )
    LOG.indent += 1
    for opt in opts:
        LOG( opt )
    LOG.indent -= 1


def escape( name ):
    return name


def pr_iterate( iterable, title, text = None, count = None ):
    return LOG.iterate( records = iterable, title = title, text = text, count = count )


def pr_table( rows ):
    LOG( utf_table.TextTable.from_table( rows, no_span = True ).to_string() )
