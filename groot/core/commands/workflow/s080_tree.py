from groot.core.utilities import gr_printing as gr_printing
from groot.mgraph import MGraph
from mhelper import io_helper
from typing import Callable, List, Optional

from mhelper.exception_helper import SwitchError
from mhelper.mannotation import isFilename, isOptional

from groot.core import constants
from groot.core.constants import EFormat, EChanges
from groot.core.data import EPosition, ESiteType, INamedGraph, Component, Model, Gene
from groot.core.utilities import AlgorithmCollection, cli_view_utils, external_runner, graph_viewing, lego_graph


DAlgorithm = Callable[[Model, str], str]
"""A delegate for a function that takes a model and aligned FASTA data, and produces a tree, in Newick format."""

tree_algorithms = AlgorithmCollection( DAlgorithm, "Tree" )


def create_trees( model: Model, algorithm: tree_algorithms.Algorithm, components: Optional[List[Component]] = None ) -> None:
    """
    Creates a tree from the component.
    Requisites: `create_alignments`
    
    cat: create
    
    :param model:       Model to use
    :param algorithm:   Algorithm to use. See `algorithm_help`.
    :param components:   Component, or `None` for all.
    
    :returns: Nothing, the tree is set as the component's `tree` field. 
    """
    # Get the site type
    if model.site_type == ESiteType.DNA:
        site_type = "n"
    elif model.site_type == ESiteType.PROTEIN:
        site_type = "p"
    else:
        raise SwitchError( "site_type", model.site_type )
    
    # Get the components
    components = cli_view_utils.get_component_list( model, components )
    
    # Assert that we are in a position to create the trees
    model.get_status( constants.STAGES.TREES_8 ).assert_create()
    assert all( x.alignment is not None for x in components ), "Cannot generate the tree because the alignment has not yet been specified."
    assert all( x.tree is None for x in components ), "Cannot generate the tree because the tree has already been generated."
    
    # Iterate the components
    for component in gr_printing.pr_iterate( components, "Generating trees" ):
        # Handle the edge cases for a tree of three or less
        num_genes = len( component.minor_genes )
        if num_genes <= 3:
            if num_genes == 1:
                newick = "({});"
            elif num_genes == 2:
                newick = "({},{});"
            elif num_genes == 3:
                newick = "(({},{}),{});"
            else:
                raise SwitchError( "num_genes", num_genes )
            
            newick = newick.format( *(x.legacy_accession for x in component.minor_genes) )
        else:
            # Run the algorithm normally
            newick = external_runner.run_in_temporary( algorithm, site_type, component.alignment )
        
        # Set the tree on the component
        set_tree( model, component, newick )
    
    # Show the completion message
    after = sum( x.tree is not None for x in model.components )
    gr_printing.pr_verbose( "{} trees generated. {} of {} components have a tree.".format( len( components ), after, len( model.components ) ) )
    model.ui_hint |= EChanges.COMP_DATA


def set_tree( model: Model, component: Component, newick: str ) -> None:
    """
    Sets a component tree manually.
    Note that if you have roots/outgroups set your tree may be automatically re-rooted to remain consistent with these settings.
    
    cat: set
    
    :param model:       Model to use
    :param component:   Component 
    :param newick:      Tree to set. In Newick format. 
                        _Gene accessions_ and/or _gene internal IDs_ may be provided.
    """
    if component.tree:
        raise ValueError( "This component already has an tree. Did you mean to drop the existing tree first?" )
    
    _force_set_tree( component, newick )
    
    model.ui_hint |= EChanges.COMP_DATA


def _force_set_tree( component, newick ):
    component.tree_newick = newick
    component.tree_unrooted = lego_graph.import_newick( newick, component.model )
    component.tree = component.tree_unrooted.copy()
    reposition_tree( component.tree )
    component.tree_unmodified = component.tree.copy()


def drop_trees( model: Model, components: Optional[List[Component]] = None ) -> None:
    """
    Removes component tree(s).
    
    cat: drop
    
    :param model:        Model to use
    :param components:   Component(s), or `None` for all. 
    """
    components = cli_view_utils.get_component_list( model, components )
    count = 0
    
    for component in components:
        if component.model.get_status( constants.STAGES.FUSIONS_9 ):
            raise ValueError( "Refusing to drop the tree because fusions have already been recorded. Did you mean to drop the fusions first?" )
        
        if component.tree is not None:
            component.tree = None
            component.tree_unrooted = None
            component.tree_newick = None
            count += 1
    
    gr_printing.pr_verbose( "{} trees removed across {} components.".format( count, len( components ) ) )
    model.ui_hint |= EChanges.COMP_DATA


def print_trees( model: Model,
                 graph: Optional[INamedGraph] = None,
                 format: EFormat = EFormat.ASCII,
                 file: isOptional[isFilename] = None,
                 fnode: str = None
                 ) -> None:
    """
    Prints trees or graphs.
    
    cat: print
    
    :param file:       File to write the output to. See `file_write_help`.
                       The default prints to the current display.
    :param graph:      What to print. See `format_help` for details.
    :param fnode:      How to format the nodes. See `print_help`.
    :param format:     How to view the tree.
    """
    if graph is None and file is None and format == EFormat.ASCII and fnode is None:
        print( "Available graphs:" )
        is_any = False
        for named_graph in model.iter_graphs():
            is_any = True
            print( type( named_graph ).__name__.ljust( 20 ) + named_graph.name )
        if not is_any:
            print( "(None available)" )
        print( "(arbitrary)".ljust( 20 ) + "(see `format_help`)" )
        model.ui_hint |= EChanges.INFORMATION
        return
    
    if graph is None:
        raise ValueError( "Graph cannot be `None` when other parameters are set." )
    
    text = graph_viewing.create( fnode, graph, model, format )
    
    with io_helper.open_write( file, format.to_extension() ) as file_out:
        file_out.write( text + "\n" )
    
    model.ui_hint |= EChanges.INFORMATION


def reposition_all( model: Model, component: Optional[Component] = None ) -> List[Component]:
    """
    Repositions a component tree based on node.position data.
    """
    if model.fusions:
        raise ValueError( "Cannot reposition trees because they already have assigned fusion events. Maybe you meant to drop the fusion events first?" )
    
    components = [component] if component is not None else model.components
    changes = []
    
    for component_ in components:
        if component_.tree is None:
            continue
        
        if component_.tree is not None and reposition_tree( component_.tree ):
            changes.append( component_ )
    
    return changes


def reposition_tree( tree: MGraph ) -> bool:
    """
    Re-lays out a tree using `LegoSequence.position`.
    """
    for node in tree:
        d = node.data
        if isinstance( d, Gene ):
            if d.position == EPosition.OUTGROUP:
                node.make_outgroup()
                return True
            elif d.position == EPosition.NONE:
                pass
            else:
                raise SwitchError( "node.data.position", d.position )
    
    return False
