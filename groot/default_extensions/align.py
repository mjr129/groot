from groot.core import Model, alignment_algorithms
from mhelper import file_helper, subprocess_helper
from mhelper.comment_helper import ignore


@alignment_algorithms.register( "muscle" )
def align_muscle( model: Model, fasta: str ) -> str:
    """
    Uses MUSCLE to align.
    """
    ignore( model )
    
    file_helper.write_all_text( "in_file.fasta", fasta )
    
    subprocess_helper.execute( ["muscle", "-in", "in_file.fasta", "-out", "out_file.fasta"] )
    
    return file_helper.read_all_text( "out_file.fasta" )


@alignment_algorithms.register( "as_is" )
def align_as_is( model: Model, fasta: str ) -> str:
    """
    Uses the FASTA as it already is.
    """
    ignore( model )
    return fasta
