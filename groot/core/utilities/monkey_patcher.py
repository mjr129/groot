import inspect


def _patch():
    """
    Groot was originally written in a non-OOP fasion.
    
    This monkey patcher translates the non OOP methods to an OOP style
    `groot.Model`. 
    """
    from groot.core import commands
    from groot.core import Model
    
    for x in dir( commands ):
        v = getattr( commands, x )
        
        if inspect.isfunction( v ):
            p = tuple( inspect.signature( v ).parameters.values() )
            
            if p and p[0].annotation is Model:
                setattr( Model, v.__name__, v )
