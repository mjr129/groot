import random
from typing import Tuple, List, cast, Optional

from groot.faketree.model.treedata import NodeData, MftNode
from groot.faketree.model import workspace
from groot.mgraph import importing
from groot.mgraph.graphing import EDirection, MEdge
from mhelper import string_helper, log_helper
from mhelper.generics_helper import ByRef


class RandomChoiceError( IndexError ):
    pass


def get_node( name: str ) -> MftNode:
    """
    Obtains a node by name.
    """
    for node in workspace.default_model.tree:  # type: MftNode
        if node.data.name == name:
            return node
    
    raise ValueError( "No such node as «{}».".format( name ) )


def new_tree( model: Optional[workspace.FtModel] = None ):
    """
    Starts a new session (clears the model).
    The model is cleared, but not parameter settings such as the pool.
    
    :param model:    Model to operate upon, `None` for the default model.
    """
    model = model or workspace.default_model
    model.reset()


def __count_dashes( text: str ) -> Tuple[str, int]:
    no_dashes = text.lstrip( "-" )
    return no_dashes, len( text ) - len( no_dashes )


def goto_node( node: MftNode, model: Optional[workspace.FtModel] = None ):
    """
    Selects a particular node so that it may be modified with the `tree` command.
    :param model:   Model to operate upon, `None` for the default model.
    :param node:    Node to select
    """
    model = model or workspace.default_model
    model.selected = node
    
    workspace.print( "Selected: {}".format( string_helper.format_array( get_path( model ) ) ) )


def copy_tree( source: MftNode, old: str = "", new: str = "" ):
    """
    Copies a tree.
    
    Note:
    Only structure and names are copied.
    The copy will not inherit the original's mutators (if set) or sequences (if created).
    
    :param source:  Source node 
    :param old:     Find in node names 
    :param new:     Replace with in node names 
    """
    source.graph.copy( nodes = source.follow().visited_nodes,
                       target = source.graph,
                       data = lambda old_data: NodeData( cast( str, old_data.name ).replace( old, new ) ) )


def print_selected( model: Optional[workspace.FtModel] = None ):
    """
    Prints the currently selected node.
    
    :param model:    Model to operate upon, `None` for the default model.
    """
    model = model or workspace.default_model
    workspace.print( "Selected: {}".format( string_helper.format_array( get_path( model ) ) ) )


def import_newick( text: str, model: Optional[workspace.FtModel] = None ) -> MftNode:
    """
    Imports a newick string into the graph.
    
    Note:
    Branch lengths are NOT imported since not all mutators can use them.
    These must be set on the mutators themselves.
    
    :param model:    Model to operate upon, `None` for the default model.
    :param text:  Text to import.
    """
    model = model or workspace.default_model
    root_ref = ByRef["MNode"]
    importing.import_newick( text, converter = NodeData, root_ref = root_ref, graph = model.tree )
    return root_ref.value


def get_random_node( node: MftNode, closed: bool = False, avoid: Optional[List[MftNode]] = None ) -> MftNode:
    """
    Obtains a random descendant of the specified node.
    
    :param avoid: Don't return these nodes. These nodes don't have to be in the candidate set.
    :param node: Specified node
    :param closed: Closed interval? (exclude `node` from the possibilities).
    :returns: The random node.
    :except RandomChoiceError: No sub-nodes
    """
    nodes = node.follow( direction = EDirection.OUTGOING ).visited_nodes
    
    if not closed:
        nodes.remove( node )
    
    if avoid:
        for x in avoid:
            if x in nodes:
                nodes.remove( x )
    
    if not nodes:
        nodes = node.follow( direction = EDirection.OUTGOING ).visited_nodes
        raise RandomChoiceError( "Cannot choose from an empty set of descendants of «{}» which are «{}» less «{}».".format( node, nodes, avoid ) )
    
    return random.choice( nodes )


def create_outgroup( nodes: List[MftNode], names: List[str] ) -> List["MftNode"]:
    """
    Creates an outgroup on a node.
    
    :param nodes:   Root(s) 
    :param names:   Name(s) of outgroup(s) 
    """
    r = []
    
    for node, name in zip( nodes, names ):
        r.append( node.add_child( data = NodeData( name ) ) )
    
    return r


LOG = log_helper.Logger( "create_random_tree", False )


def create_random_tree( names: List[str], size = 25, suffix = "a", delimiter = "", outgroup: bool = False, model: Optional[workspace.FtModel] = None ) -> List[MftNode]:
    """
    Generates a random tree or trees.
    
    :param model:       Model to operate upon.
                        `None` for the default model.
    :param outgroup:    Outgroups setting.
                        `True` - add root outgroups, the result will be the outgroups, not the roots.
    :param delimiter:   Suffix delimiter.
    :param suffix:      Suffix, `a`, `A`, `0`, `1` or your own list of characters.
    :param names:       Names of the trees (prefixed to all nodes of each tree). The number of names specifies the number of trees. 
    :param size:        Number of iterations.
    """
    results = []
    outgroups = []
    model = model or workspace.default_model
    
    for tree_name in names:
        root = model.tree.add_node( data = NodeData( "" ) )
        LOG( "Created root {}", root.__repr__() )
        results.append( root )
        leaves = set()
        leaves.add( root )
        
        if suffix == "a":
            suffix_fn = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".__getitem__
        elif suffix == "A":
            suffix_fn = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".__getitem__
        elif suffix == "0":
            suffix_fn = lambda x: str( x )
        elif suffix == "1":
            suffix_fn = lambda x: str( x + 1 )
        else:
            suffix_fn = suffix.__getitem__
        
        for _ in range( size ):
            # Choose a random leaf
            leaf: MftNode = random.choice( list( leaves ) )
            LOG( "Using leaf {}", leaf.__repr__() )
            
            # Bifurcate the leaf
            child_1 = leaf.add_child( data = NodeData( "" ) )
            child_2 = leaf.add_child( data = NodeData( "" ) )
            leaves.remove( leaf )
            leaves.add( child_1 )
            leaves.add( child_2 )
            
            LOG( "Added children {} and {}", child_1.__repr__(), child_2.__repr__() )
        
        if outgroup:
            old_edges = list( root.edges )
            outgroup = root.add_child( data = NodeData( "" ) )
            leaves.add( outgroup )
            outgroups.append( outgroup )
            LOG( "Added outgroup to root {}", outgroup.__repr__() )
            
            new_root = root.add_child( data = NodeData( "" ) )
            
            for old_edge in old_edges:
                assert isinstance( old_edge, MEdge )
                new_root.add_edge_to( old_edge.right )
                old_edge.remove_edge()
        
        # Name everything
        for leaf_index, leaf in enumerate( leaves ):
            leaf_name = suffix_fn( leaf_index )
            new_name = tree_name + leaf_name
            LOG( "Naming leaf {} to {}", leaf.__repr__(), new_name )
            leaf.data.name = new_name
            parent = leaf.parent
            
            while parent:
                if not parent.data.name:
                    new_name = tree_name + leaf_name
                else:
                    new_name = parent.data.name + delimiter + leaf_name
                
                LOG( "Naming parent {} to {}", parent.__repr__(), new_name )
                parent.data.name = new_name
                parent = parent.parent
        
        root.data.name = tree_name
    
    if outgroup:
        return outgroups
    else:
        return results


def get_path( model: workspace.FtModel ) -> List[MftNode]:
    r = []
    
    node = model.selected
    
    while node:
        r.append( node )
        node = node.parent
    
    return list( reversed( r ) )


def add_line_to_tree( names: List[str], model: Optional[workspace.FtModel] = None ):
    """
    Adds a node to the graph.
    Specify `-` in the name to indicate ancestry, e.g.
    
    `add root`
    `add -clade1`
    `add --member1`
    `add --member2`
    `add -clade2`
    `add --member3`
    `add --member4`
    
    :param model:    Model to operate upon, `None` for the default model.
    :param names:    One or more node names.
    """
    status = model or workspace.default_model
    
    for name in names:
        name, dashes = __count_dashes( name )
        
        if dashes == 0:
            new_node = status.tree.add_node( data = NodeData( name ) )
            status.selected = new_node
            workspace.print( "New root: {}".format( new_node ) )
            continue
        
        path = get_path( status )
        
        if not path:
            raise ValueError( "Cannot add «{}» - no selected node.".format( name ) )
        
        selected_dashes = len( path )
        
        delta = dashes - selected_dashes + 1
        
        if delta <= 0:
            for i in range( 0, 1 - delta ):
                path.pop()
        elif delta > 1:
            raise ValueError( "Tree syntax error. Too many indents." )
        
        last_node = path[-1]
        new_node = last_node.add_child( data = NodeData( name ) )
        status.selected = new_node
        
        workspace.print( "Selected: {}".format( string_helper.format_array( get_path( status ) ) ) )


def create_branch( parents: List[MftNode], child: MftNode ):
    """
    Specifies a branch from one node to another.
    
    :param parents:  Parent node(s) 
    :param child:   Child node
    """
    for parent in parents:
        parent.add_edge_to( child )
        workspace.print( "Branched {} --> {}".format( parent, child ) )


def remove_node( nodes: List[MftNode], drop_edges: bool = False ):
    """
    Removes nodes from the tree.
    
    :param nodes:           Nodes to drop 
    :param drop_edges:      Whether to drop the edges on these nodes.
                            By default they will be reassigned to the adjacent nodes. 
    """
    for node in nodes:
        if drop_edges:
            node.remove_node()
        else:
            node.remove_node_safely( True )
