from distutils.core import setup

def readme():
    try:
        with open( 'readme.rst' ) as f:
            return f.read()
    except Exception as ex:
        return f"Failed to read readme.rst due to an error: {ex}"

setup( name = "groot",
       url = "https://bitbucket.org/mjr129/groot",
       version = "0.0.0.62",
       description = "Generate N-rooted fusion graphs from genomic data.",
       long_description = readme(),
       author = "Martin Rusilowicz",
       license = "https://www.gnu.org/licenses/agpl-3.0.html",
       packages = ["groot",
                   "groot.core.commands",
                   "groot.core.commands.gimmicks",
                   "groot.core.commands.workflow",
                   "groot.core.data",
                   "groot.core.utilities",
                   "groot.default_extensions",
                   "groot.groot_gui",
                   "groot.groot_gui.lego",
                   "groot.groot_gui.forms",
                   "groot.groot_gui.forms.designer",
                   "groot.groot_gui.forms.resources",
                   "groot.groot_gui.utilities",
                   "groot.testing",
                   "groot.faketree",
                   "groot.faketree.commands",
                   "groot.faketree.model",
                   "groot.mgraph"
                   ],
       entry_points = { "console_scripts": ["groot = groot.groot_gui.__main__:main"] },
       
       install_requires = ["mhelper",  # MJR, general
                           "pyperclip",  # clipboard
                           "PyQt5",  # ui (GUI)
                           "sip",  # ui (GUI)
                           "biopython",
                           "six",  # Groot doesn't use this, but ete needs it
                           "mautogui", # Automatic GUI generation
                           ],
       python_requires = ">=3.7",

       classifiers = \
           [
               "Development Status :: 3 - Alpha",
        
               "Environment :: Console",
               "Environment :: Win32 (MS Windows)",
               "Environment :: X11 Applications :: Qt",
               "Environment :: MacOS X",
               "Operating System :: OS Independent",
               "Operating System :: Microsoft :: Windows",
               "Operating System :: MacOS",
               "Operating System :: POSIX :: Linux",
        
               "Intended Audience :: Science/Research",
               "Topic :: Scientific/Engineering",
               "Topic :: Scientific/Engineering :: Bio-Informatics",
               "Topic :: Utilities",
               "Topic :: Terminals",
               "Topic :: Multimedia :: Graphics :: Editors",
               "Topic :: Multimedia :: Graphics :: Editors :: Vector-Based",
               "Topic :: Multimedia :: Graphics :: Presentation",
               "Topic :: Multimedia :: Graphics :: Viewers",
        
        
               "License :: OSI Approved :: GNU Affero General Public License v3",
               "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        
               "Natural Language :: English",
               "Programming Language :: Python :: 3.6",
               "Programming Language :: Python :: 3",
               "Programming Language :: Python :: 3 :: Only",
               "Programming Language :: Other Scripting Engines"
           ]
       )
