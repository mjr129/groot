"""
Package containing Groot's GUI.

This package is invoked by Groot and is not intended for external use as a library.
"""
pass
