"""
Imports or creates the similarity matrix.

We allow the user to load an existing file or run their own algorithm.
BLAST is the default algorithm and its invocation can be found in the
`groot_ex` project. 
"""
from groot.core.utilities import gr_printing
from typing import Callable, List, Optional

from mhelper.log_helper import Logger
from mhelper.mannotation import EFileMode, isFilename

import re

from groot.core.commands.workflow.s020_sequences import _make_gene
from groot.core import Edge
from groot.core.constants import EXT_BLAST, STAGES, EChanges
from groot.core.data import Model, Domain
from groot.core.utilities import AlgorithmCollection, external_runner


LOG = Logger( "import/blast" )

DAlgorithm = Callable[[str], str]
"""
Task:
    A similarity of FASTA sequences.

Input:
    str (default): FASTA sequences for two or more genes
    
Output:
    str: A similarity matrix in BLAST format 6 TSV.
"""

similarity_algorithms = AlgorithmCollection( DAlgorithm, "Similarity" )


def create_similarities( model: Model, algorithm: similarity_algorithms.Algorithm, evalue: float = None, length: int = None ):
    """
    Create and imports similarity matrix created using the specified algorithm.
    
    cat: create
    
    :param algorithm:   Algorithm to use. See `algorithm_help`. 
    :param evalue:      e-value cutoff. 
    :param length:      length cutoff.
    """
    model.get_status( STAGES.SIMILARITIES_3 ).assert_create()
    
    input = model.genes.to_fasta()
    
    output = external_runner.run_in_temporary( algorithm, input )
    output = output.split( "\n" )
    
    __import_blast_format_6( evalue, output, "algorithm_output({})".format( algorithm ), length, model, True )


def set_similarity( model: Model, left: Domain, right: Domain ) -> None:
    """
    Adds a new edge to the model.
    
    cat: set
    
    :param model: Model to manipulate
    :param left:     Subsequence to create the edge from 
    :param right:    Subsequence to create the edge to
    """
    model.get_status( STAGES.SIMILARITIES_3 ).assert_set()
    
    edge = Edge( left, right )
    left.gene.model.edges.add( edge )
    
    model.ui_hint |= EChanges.MODEL_ENTITIES


def import_similarities( model: Model, file_name: isFilename[EFileMode.READ, EXT_BLAST], evalue: Optional[float] = 1e-10, length: Optional[int] = None ) -> None:
    """
    Imports a similarity matrix.
    If data already exists in the model, only lines referencing existing sequences are imported.
    
    cat: import
    
    :param file_name:   File to import 
    :param evalue:      e-value cutoff 
    :param length:      Length cutoff 
    :return: 
    """
    model.get_status( STAGES.SIMILARITIES_3 ).assert_create()
    
    obtain_only = model._has_data()
    
    with LOG:
        with open( file_name, "r" ) as file:
            __import_blast_format_6( evalue, file.readlines(), file_name, length, model, obtain_only )
    
    model.ui_hint |= EChanges.MODEL_ENTITIES


def drop_similarities( model: Model, edges: Optional[List[Edge]] = None ) -> None:
    """
    Detaches the specified edges from the specified subsequences.
    
    cat: drop
    
    :param edges:           Edges to affect.
                            If `None` then all edges are dropped.
    """
    model.get_status( STAGES.SIMILARITIES_3 ).assert_drop()
    
    if edges is not None:
        for edge in edges:
            edge.left.gene.model.edges.remove( edge )
    else:
        model.edges = []


def print_similarities( model: Model, find: str = "" ) -> None:
    """
    Prints model edges.
    
    cat: print
    
    :param find: If specified, only edges with accessions matching this regular expression are given.
    """
    
    if not find:
        find = ".*"
    
    f = re.compile( find )
    
    for edge in model.edges:
        if f.search( edge.left.sequence.accession ) or f.search( edge.right.sequence.accession ):
            print( str( edge ) )
    
    model.ui_hint |= EChanges.NONE

def __import_blast_format_6( e_value_tol, file, file_title, length_tol, model, obtain_only ):
    LOG( "IMPORT {} BLAST FROM '{}'", "MERGE" if obtain_only else "NEW", file_title )
    
    for index, line in enumerate( file ):
        line = line.strip()
        
        if line and not line.startswith( "#" ) and not line.startswith( ";" ):
            # BLASTN     query acc. | subject acc. |                                 | % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
            # MEGABLAST  query id   | subject ids  | query acc.ver | subject acc.ver | % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
            # Fields: 
            
            # Split by tabs or spaces 
            if "\t" in line:
                e = line.split( "\t" )
            else:
                e = [x for x in line.split( " " ) if x]
            
            if len( e ) == 14:
                del e[2:4]
            
            # Assertion
            if len( e ) != 12:
                raise ValueError( "BLAST file '{}' should contain 12 values, but line #{} contains {}: {}".format( file_title, index + 1, len( e ), repr( line ) ) )
            
            query_accession = e[0]
            query_start = int( e[6] )
            query_end = int( e[7] )
            query_length = query_end - query_start
            subject_accession = e[1]
            subject_start = int( e[8] )
            subject_end = int( e[9] )
            subject_length = subject_end - subject_start
            e_value = float( e[10] )
            LOG( "BLAST SAYS {} {}:{} ({}) --> {} {}:{} ({})".format( query_accession, query_start, query_end, query_length, subject_accession, subject_start, subject_end, subject_length ) )
            
            if e_value_tol is not None and e_value > e_value_tol:
                LOG( "REJECTED E VALUE" )
                continue
            
            if length_tol is not None and query_length < length_tol:
                LOG( "REJECTED LENGTH" )
                continue
            
            assert query_length > 0 and subject_length > 0
            
            query_s = _make_gene( model, query_accession, obtain_only, 0, True )
            subject_s = _make_gene( model, subject_accession, obtain_only, 0, True )
            
            if query_s and subject_s and query_s is not subject_s:
                query = Domain( query_s, query_start, query_end )
                subject = Domain( subject_s, subject_start, subject_end )
                LOG( "BLAST UPDATES AN EDGE THAT JOINS {} AND {}".format( query, subject ) )
                __make_edge( model, query, subject )
    
    gr_printing.pr_verbose( "Imported Blast from «{}».".format( file_title ) )


def __make_edge( model: Model, source: Domain, destination: Domain ) -> Edge:
    """
    Creates the specified edge, or returns it if it already exists.
    """
    assert source != destination
    
    for edge in model.edges:
        if (edge.left == source and edge.right == destination) \
                or (edge.left == destination and edge.right == source):
            return edge
    
    result = Edge( source, destination )
    model.edges.add( result )
    
    return result
