"""
The GUI operates on a single `Model` instance, this is it.
"""
from typing import Optional

from groot import Wizard
from mhelper import exception_helper
from groot.core.data.model import Model


__model: Model = None
__wizard: Wizard = None


def get_gui_model() -> Model:
    if __model is None:
        set_gui_model( Model() )
    
    return __model


def set_gui_model( model: Model ):
    exception_helper.safe_cast( "model", model, Model )
    global __model
    __model = model
    return __model


def get_gui_wizard() -> Wizard:
    return __wizard


def set_gui_wizard( wizard: Optional[Wizard] ) -> None:
    global __wizard
    __wizard = wizard
