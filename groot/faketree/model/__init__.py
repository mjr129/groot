"""
Defines the FakeTree model itself.
"""

from .mutators import Mutation, MutationWithPool, CombinationMutation, RandomMutation, RandomMutationRoot, SeqgenMutation, UniqueMutation, UniqueMutationRoot
from .treedata import NodeData, FtBlast, FtSequence, FtSite, FtSiteName
from .workspace import FtModel, default_model
