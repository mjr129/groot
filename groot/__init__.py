"""
This is the root of Groot, from which the Python API is exported.

The full tutorial can be found in the readme, but a good starting point is the
`groot.print_status` command. You may also wish to use `dir(groot)`
or `mhelper.debug_helper.view(groot)` to list the available commands.
"""

from groot.core import *
from mhelper.debug_helper import view as help


_ = help
