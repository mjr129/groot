from groot.core import Model
from groot.core.utilities import gr_printing as gr_printing

from groot.core.constants import STAGES, EChanges

from mhelper import utf_table


def print_status(model:Model) -> None:
    """
    Prints the status of the model.
    
    cat: printing, main
    """
    
    with gr_printing.pr_section( model.name ):
        r = [["Stage", "Content", "Status", "Suggestion"]]
        
        for index, stage in enumerate( STAGES ):
            status = model.get_status( stage )
            
            c1 = ("{}. {}:".format( index, stage.name )).ljust( 20 )
            c2 = str( status )
            
            if status.is_complete:
                c3 = "complete"
                c4 = ""
            else:
                if status.is_hot:
                    c4 = "Consider running `create_{}`".format( stage.name.lower() )
                else:
                    c4 = ""
                
                if status.is_partial:
                    c3 = "partial"
                else:
                    c3 = "incomplete"
            
            r.append( [c1, c2, c3, c4] )
        
        table = utf_table.TextTable.from_table( r ).to_string()
        gr_printing.pr_information( table )
    
    model.ui_hint|= EChanges.INFORMATION
