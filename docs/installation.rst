====================================================================================================
                                         Groot Installation                                         
====================================================================================================

----------------------------------------------------------------------------------------------------
                                           Prerequisites                                            
----------------------------------------------------------------------------------------------------

`Groot`:t: runs under Python 3.7+ and should be installed using `Pip`:t:.

* In addition to `Python`:t:, you will also need some phylogenetic tools to
  conduct the actual analysis.
* Please download the the following, install, *and confirm that they run* from
  the command line before continuing to install Groot.

=========== ======================== ===================================================
Tool        Purpose                  URL                                                
=========== ======================== ===================================================
Python 3.7+ Interpreter              https://www.python.org/downloads/                  
Pip         Installation manager     https://pip.pypa.io/en/stable/installing/          
Virtualenv  Python sandbox           https://virtualenv.pypa.io/en/latest/installation/
Blast*      Gene similarity          https://blast.ncbi.nlm.nih.gov/Blast.cgi           
Clann*      Supertree inference      http://mcinerneylab.com/software/clann/            
Muscle*     Gene alignment           https://www.ebi.ac.uk/Tools/msa/muscle/            
Paup*       Phylogeny inference      http://phylosolutions.com/paup-test/               
Raxml*      Phylogeny inference      https://sco.h-its.org/exelixis/software.html       
=========== ======================== ===================================================

.. note::

    You can substitute the bioinformatic tools (*) for your own preferred tools,
    or specify the data manually, but this list comprises a good "starter pack".

.. note::

    Make sure to place your binaries in your system PATH_ so that they can be
    accessed by Groot.

.. warning::

    For legacy reasons, `MacOS`:t: and some `Linux`:t: flavours come with an
    older version of `Python 2.4`:t: pre-installed, which was last updated in
    2006.
    This isn't enough - you'll need to install the latest `Python`:t: to use
    `Groot`:t:.

----------------------------------------------------------------------------------------------------
                                        System requirements                                         
----------------------------------------------------------------------------------------------------

`Groot`:t: isn't very fussy, it will work from your `Android`:t: phone (albeit
slowly) but if you want to use the GUI you'll need to be using a supported OS
and desktop (`Windows`:t:, `Mac`:t: and `Ubuntu`:t:+`Kde`:t: or
`Ubuntu`:t:+`Gnome`:t: are all good). See the troubleshooting_ guide if you get
into problems.


----------------------------------------------------------------------------------------------------
                        Installing Groot from source (recommended)                                     
----------------------------------------------------------------------------------------------------

This process installs Groot in editable mode - this means you can make changes
to the source code (or `git pull` a new version) without having to reinstall
anything.

First, make a copy of the source code:

    git clone https://bitbucket.org/mjr129/groot
    git clone https://bitbucket.org/mjr129/mhelper
    git clone https://bitbucket.org/mjr129/mautogui

Installing Python applications into their own sandboxed virtual environment is
always a good idea, so create a virtual environment our installation::

    python3.7 -m virtualenv grenv
    
Now, install our source code (make sure to use this order or `pip` will download
the non-editable version of the `mautogui` and `mhelper` libraries when it sees
that Groot needs them):

    grenv/bin/pip install -e mautogui
    grenv/bin/pip install -e mhelper
    grenv/bin/pip install -e groot

----------------------------------------------------------------------------------------------------
                                     Installing Groot from PyPI                                     
----------------------------------------------------------------------------------------------------

Installing Python applications into their own sandboxed virtual environment is
always a good idea, so create a virtual environment for Groot::

i.e. from a `Bash`:t: terminal... ::

    python3.7 -m virtualenv grenv
    grenv/bin/pip install groot

----------------------------------------------------------------------------------------------------
                                    Starting and stopping Groot                                     
----------------------------------------------------------------------------------------------------

Groot can be used directly from Python by using `import groot`::

    [bash]$     grenv/bin/python
    [python]$   import groot
    
You can also launch the GUI by running the `groot` executable::

    [bash]$     grenv/bin/groot
    [groot]$    ...
    
Close the main window to exit the GUI.


.. ********** REFERENCES **********

.. _PATH: https://en.wikipedia.org/wiki/PATH_(variable)
.. _`Intermake documentation`: http://software.rusilowicz.com/intermake
.. _troubleshooting: troubleshooting.rst
