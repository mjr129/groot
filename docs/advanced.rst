============================
Groot miscellaneous features
============================

---------------------------------------
Changing the way entities are displayed
---------------------------------------

Advanced GROOT options can be accessed via the `options` command in the API.

For instance, to change Fusion names to be more human readable, from the command line run::

    import groot as g
    g.fusion_namer= g.EFusionNames.READABLE
    
For a complete list of options see the `options` command help.
    


    groot local






.. webmaker default_highlight:: bash
