from typing import List, Optional

from groot.core import Gene, Model
from groot.core.data import EPosition
from groot.core.constants import EChanges


def set_outgroups( model:Model,genes: List[Gene], position: Optional[bool] = None ) -> None:
    """
    Defines or displays the position of a gene in the graph.
    If trees have been generated already they will be re-rooted.
    
    cat: set
     
    :param genes:       Gene(s) to affect, or `None` to display all genes.
    :param position:    New status.
                        `True` = "outgroup"
                        `False` = "ingroup"
                        `None` = "sole outgroup"
                        If not specified then the specified genes will be made outgroups and all others will be not.  
    """
    if position is None:
        for gene in model.genes:
            if gene in genes:
                gene.position = EPosition.OUTGROUP
            else:
                gene.position = EPosition.NONE
    else:
        for gene in genes:
            gene.position = EPosition.OUTGROUP if position else EPosition.NONE
    model.ui_hint|= EChanges.INFORMATION


def print_outgroups(model:Model,):
    """
    Prints the outgroups.
    
    cat: print
    """
    
    for gene in model.genes:
        if gene.position != EPosition.NONE:
            print( str( gene ) )
