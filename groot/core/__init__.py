"""
This is Groot's main package and API, it is rexported from the main `groot`
package.
"""

__version__ = "0.0.0.0"

from .data import *
from .commands import *

from .utilities import run_subprocess, rectify_nodes  # groot.utilities is primarily internal, though we export a few things for convenience
from .constants import STAGES, Stage, EChanges, EDomainNames, EFormat, EStartupMode, EWindowMode, EFusionNames  # groot.constants is a mix of internal and external stuff, we specify the external bits now
from .utilities.gr_file_system import storage

# noinspection PyUnresolvedReferences
import groot.default_extensions as _  # Allow the default Groot algorithm collection to register itself

from groot.core.utilities.monkey_patcher import _patch


_patch()
