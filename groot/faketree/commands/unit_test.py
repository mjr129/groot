from typing import cast

from groot.faketree.commands import mutation, tree_generation, output
from groot.faketree.commands.output import ESubset
from groot.faketree.model import workspace
from groot.faketree.model.treedata import NodeData

from mhelper import bio_helper, ansi
from mhelper.io_helper import MemoryWriter


def test_groot():
    """
    Tests the groot test creation procedure.
    """
    
    # The following is the GROOT test protocol
    args_random_tree = { "suffix": "1", "delimiter": "_", "size": 10, "outgroup": True }
    args_seqgen = "-d 0.2"
    
    outgroups = tree_generation.create_random_tree( ["A", "B", "C"], **args_random_tree )
    a, b, c = (x.parent for x in outgroups)
    
    mutation.make_seqgen( [a, b, c], args_seqgen )
    
    fa = tree_generation.get_random_node( a, avoid = outgroups )
    fb = tree_generation.get_random_node( b, avoid = outgroups )
    tree_generation.create_branch( [fa, fb], c )
    mutation.make_composite_node( [c] )
    
    output.print_trees()
    
    mutation.generate()
    
    c_start, c_end = get_blast()
    
    fasta = __get_leaf_fasta()
    true_fasta = []
    
    for accession, sequence in bio_helper.parse_fasta( text = fasta ):
        if accession.startswith( "B" ):
            true_fasta.append( ">" + accession )
            true_fasta.append( sequence )
        elif accession.startswith( "C" ):
            true_fasta.append( ">" + accession )
            true_fasta.append( sequence[c_start - 1:c_end] )
    
    graph = __make_nj_tree( "\n".join( true_fasta ), outgroups[1].data )
    
    workspace.print( graph.to_ascii( fnode = __format_node ) )


def __format_node( x ):
    if not x.has_children:
        if x.data.startswith( "B" ):
            c = ansi.FORE_RED
        elif x.data.startswith( "C" ):
            c = ansi.FORE_YELLOW
        else:
            c = ""
        
        return c + x.data + ansi.RESET
    else:
        return "--"


def get_blast():
    # We're interested in pulling out the "B" sequences
    blasts = output.get_blasts()
    
    for blast in blasts:
        if (cast( NodeData, blast.query_node.data ).name.startswith( "B" )
                and cast( NodeData, blast.subject_node.data ).name.startswith( "C" )):
            return blast.subject_start, blast.subject_end
        
        if (cast( NodeData, blast.query_node.data ).name.startswith( "C" )
                and cast( NodeData, blast.subject_node.data ).name.startswith( "B" )):
            return blast.query_start, blast.query_end
    
    raise ValueError( "Not found." )


def test_seqgen():
    """
    Tests seq-gen.
    
    cat: test
    """
    
    args_random_tree = { "suffix": "1", "delimiter": "_", "size": 10, "outgroup": True }
    args_seqgen = "-d 0.2"
    outgroup_a, outgroup_b = tree_generation.create_random_tree( ["A", "B"], **args_random_tree )
    outgroup_a_data: NodeData = outgroup_a.data
    root_a = outgroup_a.parent
    root_b = outgroup_b.parent
    
    mutation.make_seqgen( [root_a], args_seqgen )
    mutation.make_seqgen( [root_b], args_seqgen )
    
    join_point_a = tree_generation.get_random_node( root_a, avoid = [outgroup_a] )
    tree_generation.create_branch( [join_point_a], root_b )
    tree_generation.remove_node( [root_b] )
    
    output.print_trees()
    mutation.generate()
    fasta = __get_leaf_fasta()
    graph = __make_nj_tree( fasta, outgroup_a_data )
    
    print( "OUTGROUP IS {}".format( outgroup_a ) )
    print( "INPUT" )
    output.print_trees( mutator = False, length = False, clades = False )
    print( "RESULT" )
    print( graph.to_ascii( fnode = lambda x: str( x ) if not x.has_children else "--" ) )


def __get_leaf_fasta():
    output.print_fasta( which = ESubset.LEAVES, file = "memory" )
    fasta = MemoryWriter.retrieve()
    return fasta


def __make_nj_tree( fasta, outgroup_a_data ):
    # noinspection PyUnresolvedReferences,PyPackageRequirements
    import groot
    # noinspection PyPackageRequirements
    from groot.default_extensions import tree as groot_tree
    newick = groot_tree.tree_neighbor_joining( "p", fasta )
    from groot.mgraph import importing
    graph = importing.import_newick( newick )
    graph.nodes[outgroup_a_data.name].parent.make_root()
    return graph
