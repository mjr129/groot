"""
This package provides the default set of algorithms for Groot.
It is automatically `import`ed when Groot starts.

You can create your own algorithms, and `import` them manually.  
"""
from groot.default_extensions import similarity, align, domains, supertree, tree
