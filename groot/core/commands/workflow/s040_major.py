"""
Components algorithms.

The only one publicly exposed is `detect`, so start there.
"""
from typing import List, Optional

from groot.core import Model
from groot.core.utilities import gr_printing
from mhelper import string_helper
from mhelper.component_helper import ComponentFinder
from mhelper.log_helper import Logger

import warnings

from groot.core.constants import EChanges, STAGES
from groot.core.data import Component, Edge, Gene


LOG_MAJOR = Logger( "comp.major", False )
LOG_MAJOR_V = Logger( "comp.major.v", False )
LOG_GRAPH = Logger( "comp.graph", False )


def create_major( model: Model, tol: int = 0, debug: bool = False ) -> None:
    """
    Detects model components.
    
    First step of finding the components.
    
    We classify each component as a set of "major" genes.
    
    Components are defined as sets of genes that share a similarity path between them, where each edge between element 𝓧 and 𝓨 in that path:
        * Is sourced from no less than 𝓧's length, less the tolerance
        * Is targeted to no less than 𝓨's length, less the tolerance
        * The difference between 𝓧 and 𝓨's length is less than the tolerance
        
    We'll grab the minor domains that this component extends into in the next step.
    
    Requisites: Sequence similarity (BLAST data) must have been loaded 
    
    cat: create
    
    :param debug:       Assert the creation.
    :param tol:         Tolerance value
    :returns:           Nothing, the components are written to :ivar:`model.components`.
    """
    model.get_status( STAGES.MAJOR_4 ).assert_create()
    
    model.components.clear()
    
    # Find connected components
    components = ComponentFinder()
    
    # Basic assertions
    LOG_MAJOR( "There are {} sequences.", len( model.genes ) )
    missing_edges = []
    
    for sequence in model.genes:
        edges = model.edges.find_gene( sequence )
        
        if not edges:
            missing_edges.append( sequence )
    
    if missing_edges:
        raise ValueError( "Refusing to detect components because some sequences have no edges: «{}»".format( string_helper.format_array( missing_edges ) ) )
    
    # Iterate sequences
    for sequence_alpha in model.genes:
        assert isinstance( sequence_alpha, Gene )
        
        alpha_edges = model.edges.find_gene( sequence_alpha )
        any_accept = False
        
        LOG_MAJOR( "Sequence {} contains {} edges.", sequence_alpha, len( alpha_edges ) )
        
        for edge in alpha_edges:
            assert isinstance( edge, Edge )
            source_difference = abs( edge.left.length - edge.left.gene.length )
            destination_difference = abs( edge.right.length - edge.right.gene.length )
            total_difference = abs( edge.left.gene.length - edge.right.gene.length )
            
            LOG_MAJOR_V( "{}", edge )
            LOG_MAJOR_V( "-- Source difference ({})", source_difference )
            LOG_MAJOR_V( "-- Destination difference ({})", destination_difference )
            LOG_MAJOR_V( "-- Total difference ({})", total_difference )
            
            if source_difference > tol:
                LOG_MAJOR_V( "-- ==> REJECTED (SOURCE)" )
                continue
            elif destination_difference > tol:
                LOG_MAJOR_V( "-- ==> REJECTED (DEST)" )
                continue
            elif total_difference > tol:
                LOG_MAJOR_V( "-- ==> REJECTED (TOTAL)" )
                continue
            else:
                LOG_MAJOR_V( "-- ==> ACCEPTED" )
            
            if debug and edge.left.gene.accession[0] != edge.right.gene.accession[0]:
                raise ValueError( "Debug assertion failed. This edge not rejected: {}".format( edge ) )
            
            any_accept = True
            beta = edge.opposite( sequence_alpha ).gene
            LOG_MAJOR( "-- {:<40} LINKS {:<5} AND {:<5}", edge, sequence_alpha, beta )
            components.join( sequence_alpha, beta )
        
        if debug and not any_accept:
            raise ValueError( "Debug assertion failed. This sequence has no good edges: {}".format( sequence_alpha ) )
    
    # Create the components!
    sequences_in_components = set()
    
    for index, sequence_list in enumerate( components.tabulate() ):
        model.components.add( Component( model, index, sequence_list ) )
        LOG_MAJOR( "COMPONENT MAJOR: {}", sequence_list )
        sequences_in_components.update( sequence_list )
    
    # Create components for orphans
    for sequence in model.genes:
        if sequence not in sequences_in_components:
            LOG_MAJOR( "ORPHAN: {}", sequence )
            model.components.add( Component( model, len( model.components ), (sequence,) ) )
    
    # An assertion
    for component in model.components:
        assert isinstance( component, Component )
        if len( component.major_genes ) == 1:
            warnings.warn( "There are components with just one sequence in them. Maybe you meant to use a tolerance higher than {}?".format( tol ), UserWarning )
            break
    
    gr_printing.pr_verbose( "{} components detected.".format( len( model.components ) ) )
    
    model.ui_hint |= EChanges.COMPONENTS


def drop_major( model: Model, components: Optional[List[Component]] = None ) -> None:
    """
    Drops all components from the model.
    The components are removed from :ivar:`model.components`.
    
    cat: drop
    
    :param components: Components to drop. If `None` then all components are dropped. 
    """
    model.get_status( STAGES.MAJOR_4 ).assert_drop()
    
    previous_count = len( model.components )
    
    if not components:
        model.components.clear()
    else:
        for component in components:
            model.components.remove( component )
    
    gr_printing.pr_verbose( "{} components dropped".format( previous_count - len( model.components ) ) )
    
    model.ui_hint |= EChanges.COMPONENTS


def set_major( model: Model, genes: List[Gene] ) -> None:
    """
    Creates a major component (manually).
    
    cat: set 
    
    :param model: Model to manipulate
    :param genes:   Components 
    :return:            Nothing is returned, the component is added to the model. 
    """
    
    model.get_status( STAGES.MAJOR_4 ).assert_set()
    
    for gene in genes:
        if model.components.find_component_for_major_gene( gene, default = None ) is not None:
            raise ValueError( "Refusing to create a component containing the gene «{}» because that gene is already assigned to a component.".format( gene ) )
    
    model.components.add( Component( model,
                                     len( model.components ),
                                     tuple( genes ) ) )
    
    model.ui_hint |= EChanges.COMPONENTS


def print_major( model: Model, verbose: bool = False ) -> None:
    """
    Prints the major components.
    
    Each line takes the form:
    
        `COMPONENT <major> = <sequences>`
        
    Where:
    
        `major` is the component name
        `sequences` is the list of components in that sequence
        
    cat: print
        
    :param model: Model to manipulate
    :param verbose: Print verbose information (only with `legacy` parameter)
    
    """
    
    if not model.components:
        raise ValueError( "Cannot print major components because components have not been calculated." )
    
    if verbose:
        for component in model.components:
            with gr_printing.pr_section( component ):
                print( component.to_details() )
        
        model.ui_hint |= EChanges.INFORMATION
        return
    
    rows = []
    rows.append( ["component", "major elements"] )
    
    for component in model.components:
        for i, x in enumerate( component.major_genes ):
            rows.append( ["" if i else str( component ), x.accession] )
    
    with gr_printing.pr_section( "major elements of components" ):
        gr_printing.pr_table( rows )
    
    model.ui_hint |= EChanges.INFORMATION
