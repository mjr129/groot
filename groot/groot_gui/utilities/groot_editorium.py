from typing import Optional

from mautogui import qt_autogui
from mautogui.editoria import qt_editorium, str_editorium
from groot.core import storage

from groot.groot_gui.utilities import selection, gui_view

from mhelper import exception_helper
from typing import Union, Iterable, cast, Type

import re
from groot import mgraph

from groot.core.data import Component, INamedGraph, UserGraph, Domain, Gene
from groot.core.utilities import AbstractAlgorithm, AlgorithmCollection
from mhelper.comment_helper import ignore
from mhelper.exception_helper import NotFoundError
from mhelper.mannotation import ArgsKwargs, FunctionInspector


g_editorium = None
g_str_editorium = None
g_user_options = None


def get_groot_window_runner( command, parent, *, auto_close = False, confirm = False ):
    return qt_autogui.WindowedRunner( command,
                                      parent = parent,
                                      editorium = get_groot_editorium(),
                                      user_options = get_groot_user_options(),
                                      auto_close = auto_close,
                                      confirm = confirm )


def get_groot_editorium():
    global g_str_editorium
    global g_editorium
    
    if g_editorium is None:
        g_str_editorium = str_editorium.create_default()
        g_str_editorium.register( GeneCoercer() )
        g_str_editorium.register( MGraphCoercer() )
        g_str_editorium.register( ComponentCoercer() )
        g_str_editorium.register( DomainCoercer() )
        g_str_editorium.register( AlgorithmCoercer() )
        
        g_editorium = qt_editorium.create_default()
        g_editorium.default_messages.coercers = g_str_editorium
        g_editorium.register( Editor_Graph )
    
    return g_editorium


def get_groot_user_options() -> dict:
    global g_user_options
    
    if g_user_options is None:
        g_user_options = GUserOptions()
    
    return cast( dict, g_user_options )


class GUserOptions:
    def __init__( self ):
        self.data = storage.retrieve( "autogui_user_options", { } )
    
    
    def __setitem__( self, key, value ):
        self.data[key] = value
        storage.commit( "autogui_user_options", self.data )
    
    
    def get( self, key, default ):
        return self.data.get( key, default )


class Editor_Graph( qt_editorium.AbstractBrowserEditor ):
    
    
    def on_convert_from_text( self, text: str ) -> object:
        model = gui_view.get_gui_model()
        
        for graph in model.iter_graphs():
            if graph.name == text:
                return graph
        
        return None
    
    
    def on_convert_to_text( self, value: object ) -> str:
        assert isinstance( value, INamedGraph )
        return value.name
    
    
    @classmethod
    def on_can_handle( cls, info: qt_editorium.EditorInfo ) -> bool:
        return info.annotation.is_direct_subclass_of( INamedGraph )
    
    
    def on_browse( self, value: Optional[object] ) -> Optional[str]:
        ignore( value )
        r = selection.show_selection_menu( self.edit_btn, None, INamedGraph )
        
        if r is not None:
            assert isinstance( r, INamedGraph )
            return r.name


class AlgorithmCoercer( str_editorium.AbstractCoercer ):
    """
    Algorithms are referenced by their names, with parameters specified using semicolons.
    
    e.g. `dbscan`
    e.g. `kmeans;3`
    """
    
    
    def on_get_archetype( self ) -> type:
        return AbstractAlgorithm
    
    
    def on_coerce( self, info: str_editorium.CoercionInfo ) -> AbstractAlgorithm:
        type_ = cast( Type[AbstractAlgorithm], info.annotation.value )
        col: AlgorithmCollection = type_.get_owner()
        
        elements = info.source.split( ";" )
        
        algo_name = elements.pop( 0 )
        
        try:
            function = col.get_function( algo_name )
        except NotFoundError as ex:
            raise str_editorium.CoercionError( str( ex ) ) from ex
        
        args = FunctionInspector( function ).args
        arg_values = { }
        
        for index in range( len( args ) ):
            if index < col.num_expected_args:
                pass
            else:
                arg = args.by_index( index )
                arg_values[arg.name] = info.collection.coerce( elements.pop( 0 ), arg.annotation.value )
        
        return type_( function = function, name = algo_name, argskwargs = ArgsKwargs( **arg_values ) )


class MGraphCoercer( str_editorium.AbstractEnumCoercer ):
    """
    **Graphs and trees** can be referenced as the name of the object containing the graph, or as
    a format suitable for passing into `mgraph.importing.import_string` (newick, edge list, etc.)
    """
    
    
    def on_get_priority( self ):
        return self.PRIORITY.HIGH
    
    
    def on_get_archetype( self ) -> type:
        return Union[mgraph.MGraph, INamedGraph]
    
    
    def on_get_options( self, info: str_editorium.CoercionInfo ) -> Iterable[object]:
        return gui_view.get_gui_model().iter_graphs()
    
    
    def on_get_option_names( self, value: object ):
        if isinstance( value, INamedGraph ):
            return value, value.get_accid()
        elif isinstance( value, str ):
            return value
        else:
            raise exception_helper.type_error( "value", value, (INamedGraph, str) )
    
    
    def on_get_accepts_user_options( self ) -> bool:
        return True
    
    
    def on_convert_option( self, info: str_editorium.CoercionInfo, option: object ) -> object:
        if not isinstance( option, INamedGraph ):
            raise ValueError( "Return should be `INamedGraph` but I've got a `{}`".format( repr( option ) ) )
        
        if info.annotation.is_direct_subclass_of( INamedGraph ):
            return option
        else:
            return option.graph
    
    
    def on_convert_user_option( self, info: str_editorium.CoercionInfo ) -> object:
        g = mgraph.importing.import_string( info.source )
        return self.on_convert_option( info, UserGraph( g ) )


class GeneCoercer( str_editorium.AbstractEnumCoercer ):
    """
    **Sequences** are referenced by their _accession_ or _internal ID_.
    """
    
    
    def on_get_archetype( self ) -> type:
        return Gene
    
    
    def on_get_options( self, info: str_editorium.CoercionInfo ) -> Iterable[object]:
        return gui_view.get_gui_model().genes
    
    
    def on_get_option_names( self, value: Gene ) -> Iterable[str]:
        assert isinstance( value, Gene )
        return value.display_name, value.accession, value.legacy_accession, value.index


class DomainCoercer( str_editorium.AbstractEnumCoercer ):
    """
    **Domains** are referenced _in the form_: `X[Y:Z]` where `X` is the sequence, and `Y` and `Z` are the range of the domain (inclusive and 1 based).
    """
    
    RX1 = re.compile( r"^(.+)\[([0-9]+):([0-9]+)\]$" )
    
    
    def on_get_options( self, info: str_editorium.CoercionInfo ) -> Iterable[object]:
        return gui_view.get_gui_model().user_domains
    
    
    def on_get_archetype( self ) -> type:
        return Domain
    
    
    def on_convert_user_option( self, info: str_editorium.CoercionInfo ) -> object:
        m = self.RX1.match( info.source )
        
        if m is None:
            raise str_editorium.CoercionError( "«{}» is not a valid subsequence of the form `X[Y:Z]`.".format( info.source ) )
        
        str_sequence, str_start, str_end = m.groups()
        
        try:
            sequence = info.collection.coerce( Gene, str_sequence )
        except str_editorium.CoercionError as ex:
            raise str_editorium.CoercionError( "«{}» is not a valid subsequence of the form `X[Y:Z]` because X («{}») is not a sequence.".format( info.source, str_start ) ) from ex
        
        try:
            start = int( str_start )
        except ValueError as ex:
            raise str_editorium.CoercionError( "«{}» is not a valid subsequence of the form `X[Y:Z]` because Y («{}») is not a integer.".format( info.source, str_start ) ) from ex
        
        try:
            end = int( str_end )
        except ValueError as ex:
            raise str_editorium.CoercionError( "«{}» is not a valid subsequence of the form `X[Y:Z]` because Z («{}») is not a integer.".format( info.source, str_start ) ) from ex
        
        return Domain( sequence, start, end )


class ComponentCoercer( str_editorium.AbstractEnumCoercer ):
    """
    **Components** are referenced by:
        * `xxx` where `xxx` is the _name_ of the component
        * `c:xxx` where `xxx` is the _index_ of the component
    """
    
    
    def on_get_options( self, info: str_editorium.CoercionInfo ) -> Iterable[object]:
        return gui_view.get_gui_model().components
    
    
    def on_get_archetype( self ) -> type:
        return Component
    
    
    def on_get_option_names( self, value: Component ) -> Iterable[object]:
        return value, value.index
